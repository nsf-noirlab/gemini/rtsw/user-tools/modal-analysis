function [calibData] = accelCalibrate(inputData, configArray)
%[calibData] = accelCalibrate(inputData)
%   Read the accelerometer calibration and configuration files
%   and calibrate the data from units of Volts to units of m/s^2
% Inputs:
% Outputs:

% June 6 2016
% changed line: calibVals = calibVals .* 9.80665;
%           to: calibVals = 9.80665 ./ calibVals;
% it was very very wrong (over by factor of 100!!!)
% calibVals is a table of values of Volts/g.
% ex:   if reading one g with a 10V/g accelerometer, reading will be 10V.
%       10V X 9.8(m/s^2)/10(calibVal) = 9.8 m/s^2.

% July 11 2018
% Added error trap for missing serial numbers to prevent a non-intuitive
% error when accelerometers were not added to the database or a serial
% number was mistyped.

% read in the accelerometer calibration file
calibFilNam = 'AccelSerials.csv';
startRow = 3;
startCol = 1;
calib = csvread(calibFilNam,startRow,startCol);

numAccels = length(inputData(1,:)); 
% create a vector of calibration values for the accelerometers
calibVals = zeros(numAccels,1);

% configArray contains the serial numbers of the accelerometers in the
% current data set

% calib contains all the accelerometers owned by NSO along with their
% individual calibration data

% compare the accelerometer serial numbers to make sure that they are all
% in the database
missingSers = setdiff(configArray{1,2}, calib(:,1));
if ~isempty(missingSers)
    disp(' ')
    disp('The following serial numbers are not in the database:')
    disp(missingSers)
    error('aborting conversion')
end

for chnl = 1:numAccels
    % find the index of the accels in the AccelSerials.csv
    % add the calibration value to the calibVals() array.  
    calibVals(chnl) = calib(calib(:,1) == configArray{1,2}(chnl),3);
end
calibVals = 9.80665 ./ calibVals; % convert calibVals from g to m/s^2
% calibrate and scale the accelerometer data vectors
calibData = zeros(length(inputData),numAccels);
for chnl = 1:numAccels
    calibData(:,chnl) = inputData(:,chnl) .* calibVals(chnl);
end

end


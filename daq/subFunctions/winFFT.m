function [dat,fvec,s] = winFFT(timeSeries,fs,spType,winType,wSpec,wInput,chirpSt,varargin)
%HRWin perform FFT with segmented windows
%
%   Usage:
%   [dat,fvec] = winFFT(timeSeries,fs,spType,winType,wSpec,wInput,chirpSt)
%
%   INPUTS:
%   timeSeries  timeSeries vector to be transformed
%   fs          sample frequency [Hz]
%   spType      spectrum type to return (PS, LS, PSD, LSD, Fcpx)
%       PS          single sided Power Spectrum [V^2 rms]
%       LS          single sided Linear Spectrum [V]
%       PSD         single sided Power Spectral Density [V^2 /Hz]
%       LSD         single sided Linear Spectral Density [V/sqrt(Hz)]
%       Mat         return the matrix of windowed FFTs
%       Raw         return the raw 2-sided FFT
%   winType     type of window to use (HFT95 if not provided)
%   wSpec       window specifier:
%                   'len' - window length [points]
%                   'max' - single window (length set to timeSeries length)
%                   'num' - number of windows
%                   'fres'- window length is determined by freq resolution
%   wInpt       input parameter:
%                   if wSpec = len, window length
%                   if wSpec = max, N/A
%                   if wSpec = num, number of windowed segments
%                   if wSpec = fres, frequency resolution in Hz
%   chirpSt     chirp structure:
%                   contains the parameters of the chirp vector
%                   startDelay = num secs between start of chirp vector
%                       and start of active chirp (zero padding). 
%                   endDelay = num secs between end of active chirp
%                       and end of chirp vector (zero padding). 
%                   chirpTime = length of active chirp in seconds
%                   startFreq = frequency of chirp start
%                   endFreq = frequency of chirp end
%                   chirpFreq = time vector of frequency of chirp
%               
%   OUTPUTS:
%       dat         spectrum (PS, LS, PSD, LSD, Fcpx - depends on spType)
%       fvec        frequency vector[Hz] 1 or 2-sided, or zero-centered
%
%   Other important variables that may be added to the output if needed:
%       ENBW        Effective Noise Bandwidth [Hz]
%       NENBW       Normalized Equivalent Noise Bandwidth [bins]
%
%   Functions required:
%                   winGen()
%                   winSeg()
%
%   Usage:
%   Provide a continuous timeSeries vector (prefiltered for DC removal if
%   necessary), the sample frequency, the desired frequency resolution, and
%   (optionally) a window type.  The HFT95 window will be used if not
%   otherwise specified.
%   The frequency resolution is typically in the range of fs/100 to
%   fs/100,000.
%
%   Functional description:
%   The routine performs a Discrete Fourier transform using Welch's method
%   of averaging modified periodograms for obtaining a spectral density
%   estimation. See reference [2].  The frequency resolution is specified
%   by the user and the routine will achieve the lowest noise possible for
%   the specified resolution.
%   
%   The approach significantly reduces the noise in the estimated power
%   spectra by reducing frequency resolution.  In addition, windowing
%   removes artifacts caused by steps in the beginning and end of the time
%   series data.
%
%   From the estimated power spectrum, through the techniques described in
%   reference [1], the power spectral density, linear spectrum, and linear
%   spectral density are also determined to high precision, or very high
%   precision if using the high performance HFT windows.
%   Optionally, a complex two-sided FFT can be returned by specifiying the
%   Fcpx spectrum option.
%
%   Supported window types: Rectang, Hanning, Hamming, BlackmanHarris,
%   Nuttall3, Nuttall3a, Nuttall3b, Nuttall4, Nuttall4a, Nuttall4b,
%   Nuttall4c, HFT70, HFT95, HFT90D, HFT116D, HFT144D, HFT169D, HFT196D,
%   HFT223D, HFT248D.
%   These windows are described in detail in reference[1]
%
%   The HFT windows are the high-performance flat-top windows described in
%   reference [1] of the form HFTxx, where xx is the maximal sidelobe level
%   in dB and a D following means that the window is differentiable (not
%   discontinuous at the boundary) resulting in a faster side lobe dropping
%   rate at the expense of bandwidth.
%
%   Important relations:
%   LS = sqrt(PS)
%   PSD= PS/ENBW 
%   LSD = sqrt(PSD)
%
%   Written by: Bill McBride
%   24 April, 2015
%   Updated 24 Aug 2015
%   Added the tracking filter for chirp signals
%
%   29 May 2016
%   Fixed the accumulation of power spectra; it was only adding the first
%   two spectra. Changed name to winFFT3()
%
%   15 Mar 2018
%   Added wSpec input into function allowing for different window
%   specifications such as 'len','max','fres' and 'num'
%   Added switch-case allowing for variable numbers of inputs into the
%   function resulting in default settings.
%   
%   Added if statememnt to ensure fres was within mathematical limits, 
%   changing fres to a default setting and issuing a warning if limit was 
%   exceeded.
%   
%   20 July 2018
%   Added switch for tracking filter (depends on chirp strucutre being
%   present).
%
%   7 Aug 2019
%   Added 'Raw' output option

%% Sort out the variables passed to the function and fill in missing ones

narginchk(2,7); % check that number of input arguments is between 2 and 7
 
if isempty(timeSeries)
    error('winFFT Error: timeseries missing')
end
  
if isempty(fs)
    error('winFFT Error: fs missing')
end

if isempty(spType)
    disp('winFFT: no spectrum type specified - defaulting to power spectrum')
    spType = 'PS';
end

if isempty(winType)
    disp('winFFT: no window specified - defaulting to HFT95')
    winType = 'HFT95';
end

if isempty(wSpec)
    disp('winFFT: no window specifier - defaulting to single window')
    disp('beginning and end of data set may be truncated by window')
    wSpec = 'num';
end

if isempty(wInput)
    disp('winFFT: no window number specified - defaulting to single window')
    wInput = 1;
end

if nargin < 7
    trackFilt = (false);
else
    trackFilt = (true);
    if isempty(chirpSt) % check to see if chirpSt has anything in it
        error('winFFT: chirpSt is empty')
    end
    % make sure chirpSt contains all the right fields
    if ~isfield(chirpSt,{'startDelay','endDelay','chirpTime','startFreq',...
            'endFreq','chirpFreq'}) 
        error('winFFT: chirpSt is not a structure or does not contain the correct fields')
    end
end

% disp(['tracking filter = ',num2str(trackFilt)])

if strcmp(wSpec,'fres')
   maxFRes = fs/length(timeSeries);
    if wInput < maxFRes
        wInput = maxFRes;
        warning('Frequency Resolution exceeded mathematical limits and was set to %s, the mathematical limit.',maxFRes); 
    end
end   
 
%% Apply window weighting to time series
% segment data, apply window(s), and return matrix of windowed segments
% the length and number of segments depends on the desired freq resolution
[timeDat,windw,segTime] = WinSeg(timeSeries,winType,wSpec,wInput,[],fs);
[fvecLen,segs] = size(timeDat); % number of window segments and length
% disp(['windows = ',num2str(segs)]); $$$$$$$$$$$$$$$$$$$$$$$
% disp(['frequency vector Length = ',num2str(fvecLen)]);
fvec = fs/2*linspace(0,1,fvecLen/2+1)'; % single sided freq vector
%fvecfull = fs/2*linspace(0,1,fvecLen)';
dt = 1/fs;

%% Tracking Filter for Chirp Vectors
if trackFilt
    
% The tracking filter is only used for chirp signals with multiple windows.
% The tracking filter routine looks at each windowed segment, determines
% the frequency of the chirp at the beginning and end of the segment, and
% filters out all frequencies outside of that bandwidth. This significantly
% inproves the signal to noise ratio of the Fourier conversion.

% chirp parameters pre July 2018 values were hard coded as follows:
%fStart = 5; % chirp start frequency
%fEnd = 500; % chirp end frequency
%stDelay = 60; % start delay of chirp in seconds
%enDelay = 60; % end delay of chirp in seconds

% On 20 July 2018, the tracking filter was re-written using chirp values
% stored by the data acquisition computer. These values are stored in the
% chirpSt structure.
fStart = chirpSt.startFreq; % chirp start frequency
fEnd = chirpSt.endFreq; % chirp end frequency
stDelay = chirpSt.startDelay; % start delay of chirp in seconds
enDelay = chirpSt.endDelay; % end delay of chirp in seconds

%{
% old tracking filter calcs
recLen = length(timeSeries)/fs;
fRatio = fEnd/fStart;
chStTime = stDelay;
chEnTime =  recLen - enDelay;
chDur = chEnTime - chStTime;

% old filter implementation - this didn't calculate the start and end
frequencies very well, but wasn't terrible.
for segCnt = 1:segs
    % calculate filter cutoff frequencies
    %disp('tracking filter on');
    filtFreqs(segCnt,:) = fStart .* fRatio.^((segTime(segCnt,:)-chStTime)./chDur);
    if filtFreqs(segCnt,1) < fStart; filtFreqs(segCnt,1) = fStart; end;
    if filtFreqs(segCnt,2) > fEnd; filtFreqs(segCnt,2) = fEnd; end;
    % create filter objects
    hBPF = fdesign.bandpass('N,Fp1,Fp2,Ap',...
        filterOrder, filtFreqs(segCnt,1), filtFreqs(segCnt,2),...
        Apass, fs);
    HdBPF = design(hBPF, 'cheby1');
    timeDat(:,segCnt) = filter(HdBPF,timeDat(:,segCnt)); % filter the data
end
%}

% Create a matrix with the start and stop frequencies of each sweep
% segment. some of these will be zero valued because of the start and
% stop dead times. segment will normally overlap.
filtBand = zeros(segs,2);

% set up the filter
filterOrder = 10;   % Filter order (must be even for Chebyshev)
Apass  = 0.1;         % Passband Ripple (dB)

% process one segment at a time
for segCnt = 1:segs
    % find the start and end time of the windowed segment
    ts = segTime(segCnt,1); % start time of segment
    te = segTime(segCnt,2); % end time of segment
    
    % round(ts * fs) creates an index into the chirpFreq vector
    % filtBand now contains the start/stop frequencies of each segment
    filtBand(segCnt,1) = chirpSt.chirpFreq(round(ts * fs));
    filtBand(segCnt,2) = chirpSt.chirpFreq(round(te * fs));
    
    % A 0 in the start freq array and a value in the end freq means the
    % chirp started during this segment, so replace the 0 with the chirp
    % start frequency
    if filtBand(segCnt,1) == 0 && filtBand(segCnt,2) > 0
        filtBand(segCnt,1) = chirpSt.startFreq;
    end
    
    % A 0 in the end freq array and a value in the start freq means the
    % chirp finished during this segment, so replace the 0 with the chirp
    % end frequency
    if filtBand(segCnt,2) == 0 && filtBand(segCnt,1) > 0
        filtBand(segCnt,2) = chirpSt.endFreq;
    end
    
    % Two 0s in the frequency array means that a chirp was not active at
    % any point during the segment so don't filter it - it is noise so
    % replace the noise with zeros (we don't have to worry about creating
    % a step in the data because each segment is windowed.
    % If not two 0s, filter the segment.
    if filtBand(segCnt,1) == 0 && filtBand(segCnt,2) ==0
        timeDat(:,segCnt) = 0;
    else
        % The chirp filter calcs at low frequencies can create bandwidths
        % of a tiny fraction of a Hz - this isn't ideal so we fix this.
        % If f_start and f_end are closer than 1Hz, spread them out.
        if filtBand(segCnt,2) - filtBand(segCnt,1) < 1
            % find half the difference between the bandwidth and 1 Hz
            fStretch = (1 - (filtBand(segCnt,2) - filtBand(segCnt,1)))/2;
            % stretch the frequencies out to make them 1 Hz apart
            filtBand(segCnt,2) = filtBand(segCnt,2) + fStretch;
            filtBand(segCnt,1) = filtBand(segCnt,1) - fStretch;
        end
            
        % Create filter objects using frequencies determined above and
        % filter the segment.
        hBPF = fdesign.bandpass('N,Fp1,Fp2,Ap',...
            filterOrder, filtBand(segCnt,1), filtBand(segCnt,2),...
            Apass, fs);
        HdBPF = design(hBPF, 'cheby1');
        timeDat(:,segCnt) = filter(HdBPF,timeDat(:,segCnt));
    end
end

%%{
% Plot spectrogram for testing proper filter application
% reconstruct time vector from filtered timeDat matrix

plotDat = zeros(length(timeSeries),1);
for segPtr = 1:segs
    plotDat(round(segTime(segPtr,1)*fs):round(segTime(segPtr,2)*fs,1)) =...
        plotDat(round(segTime(segPtr,1)*fs):round(segTime(segPtr,2)*fs,1)) +...
        timeDat(:,segPtr);
end
scrsz = get(groot,'ScreenSize');
figure('Name','UnFiltered Time Data','NumberTitle','off',...
    'Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);
spectrogram(plotDat,10000,[],10000,fs); % filtered time data
title('UnFiltered Time Data');
%}
end

%% fft transform
F = fft(timeDat); % perform fft of timeDat matrix  
PS = zeros(fvecLen,1); % preallocate power spectrum vector
S = zeros(fvecLen,1);

% calc power spectrum of each column
for segCnt = 1:segs
    PS(:,1) = PS(:,1) + abs(F(:,segCnt)).^2;
    %S = F(:,segCnt) + S(:,1);
end

PS = PS ./ segs; % average the accumulated power spectrums
PS = 2 * PS(1:floor(fvecLen/2)+1); % truncate PS to single sided
%S = angle(S(1:floor(fvecLen/2)+1));

%% scaling factors to compensate for windowing, noise calcs
s.S1 = sum(windw); % window sum normalization parameter
s.S2 = sum(windw.*windw); % window sum squared normalization parameter
s.NENBW = fvecLen * s.S2 / s.S1^2; % normalized equivalent noise bandwidth
s.ENBW = fs * s.S2 / s.S1^2; % effective noise bandwidth

PS = PS./(s.S1^2); % apply window normaling correction

%% calculate LS, PSD, LSD, or complex if requested
if strcmp(spType,'PS')
    dat = PS;
    plotUnit = 'V^2rms'; %#ok<NASGU>
elseif strcmp(spType,'LS')
    dat = sqrt(PS); % compute linear spectrum
    plotUnit = 'V'; %#ok<NASGU>
elseif strcmp(spType,'PSD')
    dat = (PS./s.ENBW); % compute power spectral density
    plotUnit = 'V^2/Hz'; %#ok<NASGU>
elseif strcmp(spType,'LSD')
    dat = sqrt(PS./s.ENBW);
    plotUnit = 'V/sqrt(Hz)'; %#ok<NASGU>
elseif strcmp(spType,'Mat')
    dat =  F(1:floor(fvecLen/2)+1,:);
    plotUnit = 'V'; %#ok<NASGU>
elseif strcmp(spType,'Raw')
    dat = F;
    plotUnit = 'na'; %#ok<NASGU>
else
    error('unrecognized spectrum type in spType')
end

%% plot output and turn on cursor
%{
fig1 = figure(73);
semilogy(fvec,abs(dat));
title(spType);
ylabel(plotUnit);
dcm_obj = datacursormode(fig1); % enable interactive data cursor
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','off','Enable','on')

%figure(74);
%plot(fvec,angle(dat));
%}

% all done
%%  References:
%   [1] Spectrum and spectral density estimation by the Discrete Fourier
%       transform(DFT), including a comprehensive list of window
%       functions and some new flat-top windows
%       G. Heinzel, et. al., Max-Plank-Institut fur Gravitationsphysik,
%       Teilinstitut Hannover, 2002
%   [2] The Use of Fast Fourier Transform for the Estimation of Power
%       Spectra: A Method Based on Time Averaging Over Short, Modified
%       Periodograms.  Welch, P.D., IEEE Transactions on Audio
%       Electroacoustics, AU-15, 70-73, 1967
end

% notes: sometimes fs is input as 0.001 instead of 1000. This needs to be
% error checked.
function [M1] = M1imageMotion(dispDat)
%[imotVect] = M1imageMotion(dispDat)
%   Input the accelerometer displacement vectors
%   Return the complex image motion vector

% M1 sensitivity matrix in local coordinates (TN-0023 table 3.2, table 3.4)
% motion in image plane y and z in arcsec/meter displacement or
% arcsec/radian rotation
M1dxSens = [-0.364 24.169]'*1e3; % arcsec/m
M1dySens = [23.415 0.356]'*1e3; % arcsec/m
M1dzSens = [5.852 0.089]'*1e3; % arcsec/m
M1rxSens = [-410.114 -6.229]'*1e3; % arcsec/rad
M1rySens = [-6.032 398.607]'*1e3; % arcsec/rad
M1rzSens = [0 0]'*1e3; % arcsec/rad

% accelerometer channels for M1
M1x11 = 1;
M1x12 = 2;
M1x21 = 3;
M1x22 = 4;
M1x31 = 5;
M1x32 = 6; % bad accelerometer - don't use

% accelerometer locations in global coords (parked at zenith pointing)
% and converted to meters
%M1x2loc = [-2322.536 4868.138 -728.972]'*1e-3;
%M1x3loc = [-539.25 1869.222 -1478.91]'*1e-3;
M1x1loc = [1766.34 4975.56 -1669.49]'*1e-3; % from Paul
M1x2loc = [-2844.84 4975.56 -1669.49]'*1e-3;
M1x3loc = [-689.25 880.15 -1708.09]'*1e-3;

% M1 center point in global coords (parked at zenith pointing)
% (SPEC-0049 rev G, page 4)
% for some reason this is different than the global positions Paul gave me
% for the accelerometers
%M1cent = [-539.248; 3768.089; -581.473]';
M1cent = [M1x3loc(1,1),...
    M1x3loc(2,1) + (M1x1loc(2,1)-M1x3loc(2,1))/2,...
    M1x3loc(3,1) + (M1x1loc(3,1)-M1x3loc(3,1))/2]';

% lever arm length calcs from global coords(fix by considering M1 rot)&&&&
levX = M1x1loc(2)-M1x3loc(2); % length of rotational lever arm about x axis
levY = M1x1loc(1)-M1cent(1); % length of rotational lever arm about y axis
levZ = M1cent(2)-M1x3loc(2); % length of rotational lever arm about z axis

% %{
% calculate the six rigid body motions of M1
% note: all accelerometers have been calibrated to be in the plus axis
% orientation (positive displacement is positive x, y, or z)
M1.dx = 0.5*(dispDat(:,M1x22) + dispDat(:,M1x32)); % M1 x translational
M1.dy = 0.5*(dispDat(:,M1x11) + dispDat(:,M1x21)); % M1 y translational
M1.dz = 0.5*(dispDat(:,M1x12) + dispDat(:,M1x31)); % M1 z translational
M1.rx = atan((dispDat(:,M1x12)-dispDat(:,M1x31))/levX); % M1 x rotational
M1.ry = atan((dispDat(:,M1x31)-dispDat(:,M1x12))/levY); % M1 y rotational
M1.rz = atan((dispDat(:,M1x32)-M1.dx)/levZ); % M1 z rotational
% %}

%{
% for displaying individual accelerometers
M1.dx = dispDat(:,M1x11);
M1.dy = dispDat(:,M1x12);
M1.dz = dispDat(:,M1x21);
M1.rx = dispDat(:,M1x22);
M1.ry = dispDat(:,M1x31);
M1.rz = dispDat(:,M1x32);
%}

% calculate M1 image motion in y and z axes; create 2-D vector
imotY = M1dxSens(1).*M1.dx + M1dySens(1).*M1.dy + M1dzSens(1).*M1.dz +...
    M1rxSens(1).*M1.rx + M1rySens(1).*M1.ry + M1rzSens(1).*M1.rz;
imotZ = M1dxSens(2).*M1.dx + M1dySens(2).*M1.dy + M1dzSens(2).*M1.dz +...
    M1rxSens(2).*M1.rx + M1rySens(2).*M1.ry + M1rzSens(2).*M1.rz;

M1.imageMotion = complex(imotY, imotZ); % y is real part, z is imaginary part
%imotMag = abs(iMotVect); % image motion magnitude
end
%{
Notes on accelerometer placement and results:

The accelerometers were not placed properly to determine the rigid body
motions of M1 accurately; they should have been orthoginal to the axes, but
were instead in an approximate equilateral triangle configuration.
The y and z-axis translations are accurate.
The x translation is accurate but will have 1/4 of the z-axis rotation
added to it, so it will be slightly overestimated.
x and y rotations are completely mixed.  The calculations give identical
values for x and y rotation with opposite sign.  The sensitivity matrix
also has opposite signs so they are effectively added in the
calculations.
The z-axis rotation has a zero in the sensitivity matrix and is
unimportant but is calculated to facilitate analysis of the intermediate
values.
%}

function [H1,H2,Gff,Gxx,Gfx,coherence,fvec] = Hcalc(forceVect,respVect,fs,fres,win)
% Hcalc() Given a force response vector, calculate the Frequency
% Response Function (FRF) using modified windowed periodigrams. 
%   Detailed explanation goes here

% 6 June 2016
% Changed from winFFT2 to winFFT3 - winFFT2 had a bug.

% 10 July 2018
% Changed from winFFT3 to winFFT.

% Force
[F,fvec,s] = winFFT(forceVect,fs,'Mat',win,'fres',fres);

% Response
[X,~,~] = winFFT(respVect,fs,'Mat',win,'fres',fres);

[fftLen,numSegs] = size(X); % length of FFT

%{
Gff is the Fourier transform of the autocorrelation sequence of the force 
time series - aka: Force Power Spectral Density
Gxx is the Fourier transform of the autocorrelation sequence of the
response time series - aka: Response Power Spectral Density
Gfx is the cross spectrum
The individual PSD spectra returned by the FFT routine must be calculated
individually and then added and averaged.
%}
Gff = zeros(fftLen,1);
Gxx = zeros(fftLen,1);
Gfx = zeros(fftLen,1);
Gxf = zeros(fftLen,1);
for winCnt = 1:numSegs
    Gff = Gff + conj(F(:,winCnt)) .* F(:,winCnt);
    Gxx = Gxx + conj(X(:,winCnt)) .* X(:,winCnt);
    Gfx = Gfx + conj(F(:,winCnt)) .* X(:,winCnt);
    Gxf = Gxf + conj(X(:,winCnt)) .* F(:,winCnt);
end

Gff = 2 * Gff ./ (numSegs .* s.S1^2 .* s.ENBW); % PSDrms in unit^2/Hz
Gxx = 2 * Gxx ./ (numSegs .* s.S1^2 .* s.ENBW);
Gfx = 2 * Gfx ./ (numSegs .* s.S1^2 .* s.ENBW); % cross spectral density
Gxf = 2 * Gxf ./ (numSegs .* s.S1^2 .* s.ENBW);

% H estimators
% H1 is best if there is noise in the output - best estimator at
% antiresonances.
% H2 is best if there is noise in the input.
% H1<H<H2 (the true H is between them only when statistically significant)

H1 = Gfx./Gff;
H2 = Gxx./Gxf;

%{
% plot GFF, Gxx, Gfx - set pmax to enhance lower part of plot 
pmax = 5000;
figure(41);
semilogy(fvec(1:pmax,1),Gff(1:pmax,1));
title('Gff: f=sine waves 1rms + noise 1e^{-6}, r=1,0.1,0.01,0.001pk coherent');
figure(42);
semilogy(fvec(1:pmax,1),Gxx(1:pmax,1));
title('Gxx: f=sine waves 1rms + noise 1e^{-6}, r=1,0.1,0.01,0.001pk coherent');
figure(43);
semilogy(fvec(1:pmax,1),abs(Gfx(1:pmax,1)));
title('Gfx: f=sine waves 1rms + noise 1e^{-6}, r=1,0.1,0.01,0.001pk coherent');
%}

%coherence = (abs(Gxf).^2)./(Gxx.*Gff); % coherence
%coherence = abs(Gxf.*Gfx)./(Gxx.*Gff); % coherence

coherence = (abs(Gfx).*abs(Gfx))./(Gxx.*Gff); % coherence

%coherence = H1./H2; % coherence

%figure;
%plot(fvec,coherence);
end


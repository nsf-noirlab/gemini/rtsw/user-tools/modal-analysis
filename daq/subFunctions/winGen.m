function [win,ROV] = winGen(winType, N)
%Generate a window of type winType and length N
% Returns windows of type defined as a weighted sum of cosine terms.
% These widows are: Hanning, Hamming, Blackman-Harris, and flat-top
% windows.
% These windows are intended for overlapping and therefore are not
% stricly symetrical - they are missing the last term.

% by William McBride

twopi = 2*pi;
[c,ROV] = getCoeff(winType); % get weighting coefficients

win = zeros(N,1); % preallocate window vector

for j = 0:N-1 % add c(1) constant to window vector
    win(j+1) = c(1);
end

for k = 2:length(c) % add c(2) to c(n) cosine terms
    for j = 0:N-1
        win(j+1) = win(j+1) + c(k)*cos((k-1)*twopi*j/N);
    end
end

% enable for window plot:
%{
timVec = 0:1/N:1-1/N;
figure(33);
plot(timVec,win);
%}
end

function [c,ROV] = getCoeff(type)
% fetch the coefficients for the window

switch type
    case 'Rectang'
        c = 1;
        ROV = 0;
    case 'Hanning'
        c = [0.5 -0.5];
        ROV = 50;
    case 'Hamming'
        c = [0.54 -0.46];
        ROV = 50;
    case 'BlackmanHarris'
        c = [0.35875 -0.48829 0.14128 -0.01168];
        ROV = 66.1;
    case 'Nuttall3'
        c = [0.375 -0.5 0.125];
        ROV = 64.7;
    case 'Nuttall3a'
        c = [0.40897 -0.5 0.09103];
        ROV = 61.2;
    case 'Nuttall3b'
        c = [0.4243801 -0.4973406 0.0782793];
        ROV = 59.8;
    case 'Nuttall4'
        c = [0.3125 -0.46875 0.1875 -0.03125];
        ROV = 70.5;
    case 'Nuttall4a'
        c = [0.338946 -0.481973 0.161054 -0.018027];
        ROV = 68.0;
     case 'Nuttall4b'
        c = [0.355768 -0.487396 0.144232 -0.012604];
        ROV = 66.3;
    case 'Nuttall4c'
        c = [0.3635819 -0.4891775 0.1365995 -0.0106411];
        ROV = 65.6;
    case 'HFT70'
        c = [1 -1.90796 1.07349 -0.18199];
        ROV = 72.2;
    case 'HFT95'
        c = [1 -1.9383379 1.3045202 -0.4028270 0.0350665];
        ROV = 75.6;
    case 'HFT90D'
        c = [1 -1.942604 1.340318 -0.440811 0.043097];
        ROV = 76.0;
    case 'HFT116D'
        c = [1 -1.9575375 1.4780705 -0.6367431 0.1228389 -0.0066288];
        ROV = 78.2;
    case 'HFT144D'
        c = [1 -1.96760033 1.57983607 -0.81123644 0.22583558 -0.02773848...
            0.00090360];
        ROV = 79.9;
    case 'HFT169D'
        c = [1 -1.97441842 1.65409888 -0.95788186 0.33673420...
            -0.06364621 0.00521942 -0.00010599];
        ROV = 81.2;
    case 'HFT196D'
        c = [1 -1.979280420 1.710288951 -1.081629853 0.448734314...
            -0.112376628 0.015122992 -0.000871252 0.000011896];
        ROV = 82.3;
    case 'HFT223D'
        c = [1 -1.98298997309 1.75556083063 -1.19037717712...
            0.56155440797 -0.17296769663 0.03233247087 -0.00324954578...
            0.00013801040 -0.00000132725];
        ROV = 83.3;
    case 'HFT248D'
        c = [1 -1.985844164102 1.791176438506 -1.282075284005...
            0.667777530266 -0.240160796576 0.056656381764...
            -0.008134974479 0.000624544650 -0.000019808998 0.000000132974];
        ROV = 84.1;
        
    otherwise
        error('Window type not recognized');
end
end
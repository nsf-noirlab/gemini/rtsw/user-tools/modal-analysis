function hAx = spectPlot(fileName,dec,fRes,winSpec,datTim,freqMax,plotData,plotLocation)
%spectPlot Plot spectrogram. Called from createSpectrogram
%   Detailed explanation goes here

%% set up for the spectrogram inputs
% spectrogram is of the form:
% [s,f,t] = spectrogram(x,window,noverlap,f,fs)

% number of fft points and overlap
% determined by desired frequency resolution: fRes rounded up to next power
% of 2 for effecient DFT processing
nfft = 2^nextpow2(dec.fs/fRes);
fResNew = dec.fs/nfft; % actual freq resolution
disp(['fft length = ',num2str(nfft),...
  ' bins, frequency resolution = ',num2str(fResNew), ' Hz'])
[win, ROV] = winGen(winSpec, nfft);
noverlap = (ROV/100) * nfft;
noverlap = cast(noverlap, 'int32');

%% plot it already
disp('plotting')
% Position is [left, bottom, width, height] in pixels
% size is:
wdth = 1024;
hgth = 768;
figure('Position', [48 512 wdth hgth]);
hAx = gca;
spectrogram(sqrt(plotData.r), win,noverlap,nfft,dec.fs,'power','yaxis');
colormap jet;
title(hAx,[plotLocation,' Vibration Magnitude ', datTim])
ylim(hAx,[0,freqMax])
% name of plot into axis.UserData to identify the plot
hAx.UserData.Name = fileName;

hDt = datatip(hAx.Children,1,1);
set (hDt, 'Visible','off')
set (hDt, 'ValueChangedFcn',@spectPlotLine)

% duration vector of the image time vector (it is in hours (I hope!))
testDur = hours(hAx.Children.XData);
% find start time of test
realTime = datetime(datTim);
% add them for the test time vector
testTime = realTime+testDur;
% Unfortunately, we can't put datetimes into the plot - it wants numbers in
% sequential order. So we have to put them in manually

% find the index of the hour changes
hourIndex = find(ischange(hour(testTime)));
hourIndex = hourIndex';
% find the value of the x-data at those indices
xVals = (hAx.Children.XData(hourIndex));

% take control of the x ticks
xticklabels('manual')
% put the x ticks here:
xticks(hAx, xVals);
% create the labels for the x ticks by stripping out the hours and minutes
xticklabs = string(testTime(hourIndex),'H:mm');
% overwrite the x tick labels
xticklabels(hAx,xticklabs');

end


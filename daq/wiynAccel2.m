function wiynAccel
% data acquisition with DT9857E
% William McBride 19 Dec 2019
% Got it working 24 Feb 2020 after a long break
% to do:
% add the vector calcs
% add functionality to kill plotting
% add spectrogram option
% add ErrorOccured callback
%
% Jan 2021
% William McBride
% Upgraded to Matlab 2021a which broke the interface to the DT9857E
% renamed the new version wiynAccel2.mat
% re-wrote the interface to work with Matlab's new way of doing things.
% This didn't entirely work. Contacted Matlab but didn't get any response.
% I could roll back, but will wait until Matlab fixes this.
%
% 22 April 2021
% William McBride
% Matlab did some fixes.
% The program now runs, but had an error about missing .dll files
% but found https://kb.mccdaq.com/KnowledgebaseArticle50838.aspx
% added the .dll files. Program runs again.
% added if statement to prevent fft plot from showing up when not in fft
% mode. Reset timing so buffer is saved every minute and plots are 1
% second.
% 


%% SETUP
close all;
disp('Starting Accelerometer Acquisition')
disp('This takes several minutes after a reboot')
% set sample rate
% DT9857E A/D rates are 195.3125 Hz to 105.469 kHz
fs = 1000;
configFileName = 'WIYN_config.csv'; % file with accelerometer config
% set mode to 'save' for recording a raw data set
% set mode to 'fft' for displaying FFT plots and not saving the data
mode = 'save';
%mode = 'fft';

%% initialize variables
% Buffers to accumulate data packets from s.DataAvailable events 
cumData = [];
cumTime = [];
% Numer of events to accumulate before saving the buffer to a file and
% starting a new data accumulation.
eventBufferSize = 60; % ####################################### 120
% Counter for s.DataAvailable events
dataEventCount = 0;
bufferCount = 1;
% get and display current date and time
datTimNam = datestr(clock, 'yyyymmdd_HH_MM_SS');
datTim = datestr(clock, 'mmmm dd, yyyy HH:MM:SS.FFF AM');

% get accel info from database
fname = strcat(pwd,'\configFiles\',configFileName);
accelArray = AccelConfig(fname);
ActiveCh = accelArray{1,1}';

%% CREATE DIRECTORY & FILENAME
% make directory that includes date and time
newFolder = ['wvib_',datTimNam];
[dirStat,dirErrMsg,dirErrMsgid] = mkdir(newFolder);
if ~dirStat
  disp(dirErrMsg);
  disp(dirErrMsgid);
  return;
end
  
%% CLEAR ANY OLD DATA BUFFERS IN THE CURRENT DIRECTORY
delete('dataBuffer*.mat');
  
%% CREATE PLOTS
% Set up plot for data capture channels
numChannels = length(accelArray{1,1});
switch numChannels
case 2
  pltSz = [1 ,2];
case 3
  pltSz = [2 ,2];
case 4
  pltSz = [2 ,2];
case 5
  pltSz = [2 ,3];
case 6
  pltSz = [2 ,3];
case 7
  pltSz = [3 ,3];
case 8
  pltSz = [3 ,3];
case 9
  pltSz = [3 ,3];
case 10
  pltSz = [4 ,3];
case 11
  pltSz = [4 ,3];
case 12
  pltSz = [4 ,3];
case 13
  pltSz = [5 ,3];
case 14
  pltSz = [5 ,3];
case 15
  pltSz = [5 ,3];
case 16
  pltSz = [6 ,3];
case 17
  pltSz = [6 ,3];
case 18
  pltSz = [6 ,3];
otherwise
  disp('Error: number of channels invalid');
end

% plot line colors
colors = get(groot,'defaultAxesColorOrder');

% Time plot
hAccelFig = figure('Position',[0 0 1024 768]);
% need to preallocate the axes objects; zeros seem to work fine
htAx = zeros(numChannels,1); 
% create the subplot axes
for i = 1:numChannels
  htAx(i) = subplot(pltSz(1),pltSz(2),i,...
    'NextPlot', 'replacechildren','XGrid','on','XMinorGrid','on',...
        'YGrid','on','YMinorGrid','on');
end
% add the titles and axes labels
for i = 1:numChannels  
  title(htAx(i),accelArray{1,3}(i,1));
  ylabel(htAx(i),strcat(accelArray{1,5}(i,1),...
  accelArray{1,4}(i,1),' m/s^2'));
  xlabel(htAx(i),'Time (s)');
end
set(htAx,'FontSize', 10); % for all plots

% FFT plot
if strcmp(mode, 'fft')
  hFFTFig = figure('Position',[500 0 1024 768]);

  hfAx = zeros(numChannels,1);
  for i = 1:numChannels
    hfAx(i) = subplot(pltSz(1),pltSz(2),i,...
      'NextPlot', 'replacechildren','XGrid','on','XMinorGrid','on',...
      'YGrid','on','YMinorGrid','on');
  end
  for i = 1:numChannels  
    title(hfAx(i),accelArray{1,3}(i,1));
    ylabel(hfAx(i),strcat(accelArray{1,5}(i,1),...
    accelArray{1,4}(i,1),'Displacement (m)'));
    xlabel(hfAx(i),'Frequency (Hz)');
  end
  set(hfAx,'FontSize', 10); % for all plots
end

%% Start session
d=daq("dt");
daqlist("dt")

% sample frequency
d.Rate = fs;

% Set count to fs so one second of data is recorded
d.ScansAvailableFcnCount = fs;

% Add the channels
% ActiveCh is an array of channel numbers. They may not be in order or may
% not be sequential.
% chIdx is a sequential array of AnalogInputIEPEChannels.

chCount = 0;
for i = ActiveCh
  chCount = chCount + 1;
  ch = addinput(d,"DT9857E-16(00)", ActiveCh(chCount), "IEPE");
  ch.Range = [-10 10];
  ch.TerminalConfig = "SingleEnded";
  ch.Coupling = "AC";
  ch.ExcitationCurrentSource = 'Internal';
  ch.ExcitationCurrent = 0.004;
  chIdx(chCount) = ch;
end


d
% callback fucntion (runs when number in ScansAvailableFcnCount is reached)
d.ScansAvailableFcn = @dataAccumulate;
%d.ScansAvailableFcn = @(src,evt) dataAccumulate;

% run in background so MATLAB can do other things while collecting data
start(d,"Continuous");
%pause(3);
%d

% loop forever here until user presses a key
prompt = ['Press return to stop ', newline];
input(prompt);

%% Stop session and clear from memory
d.stop();
delete (d);

%% Save and concatinate all the buffers if mode = 'save'
% Aquisition stopped so save the last data and time variables
if strcmp(mode, 'save')
  save('dataBufferLast', 'cumData', 'cumTime');

  % Concatinate databuffer files
  bufferCount = 1;
  BfrName = 'dataBuffer0001.mat'; % first buffer filename
  dataLast = [];
  timeLast = [];

  % load each buffer one at a time and concatinate them into cumData and
  % cumTime variables
  while (exist(strcat(pwd,'/',BfrName),'file') == 2)
    disp(['loading dataBuffer', num2str(bufferCount,'%04u'),'.mat']);
    load(strcat(pwd,'/',BfrName), 'cumData', 'cumTime');   
    if (bufferCount == 1)
      dataLast = cumData;
      timeLast = cumTime;
    else
      dataAll = vertcat(dataLast, cumData);
      timeAll = vertcat(timeLast, cumTime);
      dataLast = dataAll;
      timeLast = timeAll;
    end  
    bufferCount = bufferCount + 1;
    BfrName = ['dataBuffer', num2str(bufferCount,'%04u'),'.mat'];
  end

  % add remaining partial data file if it exists
  if (exist(strcat(pwd,'/','dataBufferLast.mat'),'file') == 2)
    load(strcat(pwd,'/','dataBufferLast.mat'), 'cumData', 'cumTime');
    dataAll = vertcat(dataLast, cumData);
    timeAll = vertcat(timeLast, cumTime); 
  end

  % create structure and save with data and time variables
  sh.data = dataAll;
  sh.time = timeAll;
  sh.datTim = datTim;
  sh.accelArray = accelArray;
  sh.fs = fs;
  sh.notes = ['The first columns of sh.data are the accelerometer data ',...
  '(see sh.accelArray)',...
  'The next column is the force sensor signal in raw units.',...
  '(multiply by sh.FSCalib to obtain units of Newtons)',...
  'The last column is the recorded drive signal in Volts',...
  'sh.time is the time vector.',...
  'sh.source is the Matlab generated drive signal.',...
  'sh.fs is the sample frequency.',...
  'sh.datTim is the date and time.',...
  'sh.windSpeed is the wind speed in m/s.',...
  'the filename is SHKR_ followed by the date as YYYYMMDD_hh_mm_ss.' ];

  save([newFolder,'/',newFolder],'sh','-v7.3'); % force v7.3 for >2GB
  disp(['File saved to ',newFolder]);
  delete('dataBuffer*.mat')
end

function dataAccumulate(obj,evt)
  %disp('dataAccumulate called')
  [scanData,timeStamp,~] = read(obj,obj.ScansAvailableFcnCount,"OutputFormat","Matrix");
  dataEventCount = dataEventCount + 1; % count packets
  cumData = vertcat(cumData, scanData); % accumulate packets
  cumTime = vertcat(cumTime, scanData);

% Plot the data in each subplot
colCntr = 1; % counter for system default colors 1-7
for index = 1:numChannels
  plot(htAx(index), timeStamp, scanData(:,index),...
    'Color', colors(colCntr,:));            
  colCntr = colCntr+1;
  if colCntr > 7
    colCntr = 1;
  end
end

% deal with data in the buffer if it is full
if dataEventCount >= eventBufferSize
  % the buffer is full so either save it or do some analysis on it
  % depending on mode
  if strcmp(mode, 'save')
    % number filename
    fileName = ['dataBuffer',num2str(bufferCount,'%04u')]; 
    disp(['saving file ',fileName]);
    save(fileName, 'cumData', 'cumTime');
    disp([fileName ' has been saved.']);
    
  elseif strcmp(mode,'fft')
    assignin('base','timeVars',cumData);
    %getVectors(fs, event.Data(:,1),event.Data(:,2),event.Data(:,3)); % ##########################3
    % Plot the data in each subplot
    colCntr = 1; % counter for system default colors 1-7
    for index = 1:numChannels
      % double integrate the time data
      [velDat, dispDat] = discIntgrt(cumData,fs,1);
      % perform FFT on time data
      [plotData,fvec,~] = winFFT(dispDat,fs,'LS',...
        'BlackmanHarris','max',1);
 
      % only plot from 1 to 50 Hz
      maxFreq = fvec(end,1);
      stFreq = find(fvec==1);
      enFreq = find(fvec==50);
      %assignin('base','fvec',fvec);
      %fvecLen = floor(length(fvec)/100); %plot only to max freq /2
      semilogy(hfAx(index), fvec(stFreq:enFreq,1), plotData(stFreq:enFreq,1),...
          'Color', colors(colCntr,:));                                   
      colCntr = colCntr+1;
      if colCntr > 7
        colCntr = 1;
      end
      
    end
    
  else
    error('unknown mode');
  end
  
  % clear the buffers for the next event
  bufferCount = bufferCount + 1; %increment buffer counter
  dataEventCount = 0; % reset data event counter
  cumData = []; % empty buffers and continue
  cumTime = [];

%  for index = 1:numChannels
%    cla(htAx(index));
%  end
 
end
end % end data accumulate fcn

%% Local function to read acceleration .csv file
function [accelArray] = AccelConfig(filename)
%{
Read configuration file and create cell array of accelerometer data
Append calibration data from AccelSerials.csv file with calls to
AccelCalib().
Return accelArray
accelArray columns are: {1:DAQ ch, 2:serial number, 3:location name,
4:location axis, 5:polarity of axis, 6:calibration value}
%}

% Initialize variables.
delimiter = ',';
startRow = 4;

% Format string for each line of text:
%   column1: unsigned integer 8-bit
%	column2: unsigned integer 16-bit
%   column3: text (%s)
%	column4: text (%s)
%   column5: text (%s)
% DAQ ch., serial number, location, axis, polarity
formatSpec = '%u8%u16%s%s%s%[^\n\r]';
%formatSpec = '%s%s%s%s%s%[^\n\r]';

% Open the text file
fileID = fopen(filename,'r');

% Read columns of data according to format string
accelArray = textscan(fileID,formatSpec,...
    'Delimiter',delimiter,...
    'ReturnOnError',false,...
    'HeaderLines',startRow-1);

% Close the text file
fclose(fileID);

% number of accelerometers
numAccels=length(accelArray{1,1});

% accelerometer sensitivity values
sen = zeros(numAccels,1);

for AccelCounter=1:numAccels
    %sn = serial number from i row of accelArray
    sn = accelArray{1,2}(AccelCounter,1); 
    %call AccelCalib to get sensitivity value from AccelSerials.txt
    calibVal = AccelCalib(sn);
    % fill 'sen' array with sensitivity values
    sen(AccelCounter,1) = calibVal;
end

% append sen array to accelArray
accelArray{:,6} = sen;
end % end local function

  %% Local function to read accelerometer calibration .csv file
  function [calib] = AccelCalib(serialNum)
  % Read accelerometer information from AccelSerials.csv
  % Text file format: Model, serial number, sensitivity (V/g)
  % input: serial number
  % output: sensitivity returned in variable calib
    
  % Initialize variables.
  filename = strcat(pwd,'\configFiles\AccelSerials.csv');
  delimiter = ',';
  startRow = 4;

  % Format string for each line of text:
  % Model, Serial number, sensitivity (V/g)
  %	column2: double (%f)
  %   column3: double (%f)
  formatSpec = '%*s%f%f%[^\n\r]';

  % Open the text file.
  fileID = fopen(filename,'r');

  % Read columns of data according to format string.
  calibData = textscan(fileID,formatSpec,...
    'Delimiter',delimiter,...
    'HeaderLines',startRow-1,...
    'ReturnOnError',false);

  % Close the text file.
  fclose(fileID);

  % Find number of rows in matrix
  numUnits=length(calibData{1,1}(:,1));

  % set calib to zero
  % function will return 0 if serial number not found in file
  calib = 0;

  for unitCounter=1:numUnits
    if serialNum == calibData{1,1}(unitCounter,1)
        calib = calibData{1,2}(unitCounter,1);
    end
  end
  
  end

end % end local function


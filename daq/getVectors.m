function getVectors(Fs,x,y,z)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% obtain mag-squared coherence estimate using averaged coherence estimator
% (otherwise it will be 1 for all frequencies - not sure why but it is
% described in:
% https://www.mathworks.com/help/signal/ug/cross-spectrum-and-magnitude-squared-coherence.html
winLen = 1000;
win = blackman(winLen);

freqs = 1:0.1:50;
[Cxy,F] = mscohere(x,y,win,80,freqs,Fs);
[Cxz,~] = mscohere(x,z,win,80,freqs,Fs);
[Cyz,~] = mscohere(y,z,win,80,freqs,Fs);

%plot coherence
%%{
figure(1);
hCoh = gca;
plot(hCoh,F,Cxy);
hold on;
plot(hCoh,F,Cxz)
plot(hCoh,F,Cyz)
hold off;
title('Magnitude-Squared Coherence')
xlabel('Frequency (Hz)')
legend('x-y','x-z','y-z')
grid
%%}

% Create boolean vector for for the purpose of eliminating low-level
% non-coherent data points
cohThres = 0.5;
CxyMask = Cxy > cohThres;
CxzMask = Cxz > cohThres;
CyzMask = Cyz > cohThres;

% PSD of the x, y, and z accelerometers
[Pxx,~] = pwelch(x,win,80,freqs,Fs);
[Pyy,~] = pwelch(y,win,80,freqs,Fs);
[Pzz,~] = pwelch(z,win,80,freqs,Fs);

% width of frequency bin in Hz
fv=mean(diff(F));

% get linear spectrum from auto-spectrum
% take sqrt to go from power spectrum to linear spectrum
% multiply by fv to go from PSD to PS
% multiply by sqrt(2) to get peak amplitude from RMS
% multiply by 2 to go from 2 sided to 1 sided spectrum
% A is in units of meters of displacement
Ax = sqrt(Pxx.*(fv*sqrt(2)*2));
Ay = sqrt(Pyy);
Az = sqrt(Pzz);

% find peaks in linear spectrum (LS)
[pksX,locX] = findpeaks(Ax);
[pksY,locY] = findpeaks(Ay);
[pksZ,locZ] = findpeaks(Az);

% create vector including all peak locations from all 3 LS vectors
locAll = union(union(locX,locY),locZ);
% total number of peaks found
numPks = length(locAll);

%% Create Matrix of Peaks
% create an empty matrix with columns:
%   bin #, frequency(Hz), LS: x, y, z, CMask: xy, xz, yz
pkMat = NaN(numPks,8);

% fill in column 1: bin #
pkMat(:,1) = locAll;

% find the frequencies of the peaks and put into column 2 of pkMat
pkMat(:,2) = F(pkMat(:,1));

% put the linear spectrum amplitudes into columns 3,4,5 of pkMat
pkMat(:,3) = Ax(1,locAll);
pkMat(:,4) = Ay(1,locAll);
pkMat(:,5) = Az(1,locAll);

% columns 6,7,8 contain booleans of the peaks identified
% zero out the columns
pkMat(:,6:8) = 0;

% index into pkMat where Lx, Ly, Lz values are located
[~,xIdx] = intersect(pkMat(:,1),locX);
[~,yIdx] = intersect(pkMat(:,1),locY);
[~,zIdx] = intersect(pkMat(:,1),locZ);

% put the booleans into columns 6,7,8 of the pkMat array
% these identify the peaks that should be included
pkMat(xIdx,6) = 1;
pkMat(yIdx,7) = 1;
pkMat(zIdx,8) = 1;

% put the coherence booleans into columns 9,10,11 the pkMat array
pkMat(:,9) = CxyMask(1,locAll);
pkMat(:,10) = CxzMask(1,locAll);
pkMat(:,11) = CyzMask(1,locAll);

% plot linear-spectrum
figure(4);
hPs = gca;
plot(hPs,F,Ax,'LineWidth',1)
hold on;
plot(hPs,F,Ay,'LineWidth',1)
plot(hPs,F,Az,'LineWidth',1)
plot(hPs,F,Cxy.*CxyMask,'LineWidth',4)
plot(hPs,F,Cxz.*CxzMask,'LineWidth',4)
plot(hPs,F,Cyz.*CyzMask,'LineWidth',4)
hold off;
hPs.YScale = 'log';
title('Linear Spectrum')
xlabel('Frequency (Hz)')
ylabel('Amplitude')
legend('Ax','Ay','Az','xyCoh','xzCoh','yzCoh')
grid

ref0 = zeros(numPks,1);

% plot 3D vector
figure(5);
hAxq = gca;
quiver3(hAxq,ref0,ref0,ref0,pkMat(:,3),pkMat(:,4),pkMat(:,5));
hAxq.FontSize = 12;
hAxq.XLabel.String = 'X-axis (\mum)';
hAxq.YLabel.String = 'Y-axis (\mum)';
hAxq.ZLabel.String = 'Z-axis (\mum)';
hAxq.Title.String = 'Vibration Peak Amplitudes & Direction';

% peak frequency strings
fstr = cell(numPks,4);
for i = 1:numPks
  fstr(i,1) = {[num2str(round(pkMat(i,2),1)),' Hz ']};
  fstr(i,2) = {['x: ',num2str(round(pkMat(i,3).*1e6))]};
  fstr(i,3) = {['y: ',num2str(round(pkMat(i,4).*1e6))]};
  fstr(i,4) = {['z: ',num2str(round(pkMat(i,5).*1e6))]};
  %fstr(i,1) = st1,st2,st3,st4};
  text(pkMat(i,6),pkMat(i,7),pkMat(i,8),fstr(i,:),'FontSize',12,'HorizontalAlignment',...
    'right','VerticalAlignment','top','Margin',1)
  assignin('base','fstr',fstr);
end

end


% create a spectrogram from long file of accelerometer data
% Bill McBride
% 17 Nov 2020

% to do:
% calibrate spectrogram into meaningful units

close all;
% for normal operation: bypassFile = 'off'
% turn bypassFile 'on' to bypass file loading and processing
% (must have already loaded a file)
bypassFile = 'off';

if strcmp(bypassFile,'on')
  disp('debug mode')
else
  % start with clean workspace
  clear;
  %% Setup the plot here
  % hint: to increase time resolution, set frequency resolution lower
  freqMax = 50; % max frequency to plot
  fRes = 0.05; % desired frequency resolution (FFT bin size) in Hz.
  winSpec = 'HFT95'; % window for FFT
  % available windows are:
  % Rectang, Hanning, Hamming, BlackmanHarris, Nuttall3, Nuttall3a,
  % Nuttall3b, Nuttall4, Nuttall4a, Nuttall4b, Nuttall4c, HFT70, HFT95,
  % HFT90D, HFT116D, HFT144D, HFT169D, HFT196D, HFT223D, HFT248D
  % load file using ui - all file data is contained in structure 'sh'
  [sh, fileName] = spectLoad;
  % process file
  [dec,datTim,OSS,MOS,CAS] = spectProc(sh,freqMax);
  clear sh
end

% plot spectrogram
% return axis handle
hAxOSS = spectPlot(fileName,dec,fRes,winSpec,datTim,freqMax,OSS,'OSS');
hAxMOS = spectPlot(fileName,dec,fRes,winSpec,datTim,freqMax,MOS,'MOS');
hAxCAS = spectPlot(fileName,dec,fRes,winSpec,datTim,freqMax,CAS,'CAS');


%% save plots
%%{
disp('saving plots')
% save the plots as xxx.png plots
% file name is the original data file with _xxHz_yyy.png
% where xx is the max plot frequency in Hz and yy is the location

exportgraphics(hAxOSS.Parent, [hAxOSS.UserData.Name,'_',num2str(freqMax),'Hz_OSS.png'])
exportgraphics(hAxMOS.Parent, [hAxMOS.UserData.Name,'_',num2str(freqMax),'Hz_MOS.png'])
exportgraphics(hAxCAS.Parent, [hAxCAS.UserData.Name,'_',num2str(freqMax),'Hz_CAS.png'])
%%}

%% create a datatip
%dt = findobj(hMosAx.Children,'Type','datatip');


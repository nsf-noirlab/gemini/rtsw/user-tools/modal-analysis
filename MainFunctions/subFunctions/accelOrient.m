function [outputData] = accelOrient(inputData, configArray)
%[calibData] = accelCalibrate(inputData, configArray)
%   Read the accelerometer orientation from configArray
%   and apply the orientation to the accelerometer data

polarity = configArray{5}; % cell array containing '+' or '-'
numAccels = length(polarity);
polSign = zeros(1,numAccels);

% generate an array containing either 1 or -1 to multiply with the
% accelerometer data
for arrayCnt = 1:numAccels
    if strcmp(polarity(arrayCnt),'+')
        polSign(1,arrayCnt) = 1;
    elseif strcmp(polarity(arrayCnt),'-')
        polSign(1,arrayCnt) = -1;
    else
        error('Undefined value in accelerometer polarity array');
    end
end

% multiply the above array with the accelerometer data
outputData = zeros(size(inputData));
for clmnCnt = 1:numAccels
    outputData(:,clmnCnt) = inputData(:,clmnCnt) .* polSign(:,clmnCnt);
end
end


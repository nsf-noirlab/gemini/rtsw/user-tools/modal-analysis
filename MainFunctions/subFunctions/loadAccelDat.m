function [dataSets] = loadAccelDat()
%loadAccelDat: Open a dialog box for user to select files. Load the data
%from the files and return a structure of the data sets.
%   Bill McBride
%   26 May 2015
filterIndex = 1;
fileCnt = 0;
FileArray = {0}; % allocate cell array
while filterIndex
    [fileName,pathName,filterIndex] = uigetfile('*.mat',...
        'Select Shaker Data File.  Cancel to End');
    if ~filterIndex         % No file was selected
        break;
    end 
    fileCnt = fileCnt + 1;
    FileArray{fileCnt,1} = [pathName fileName];
end

for loadCnt = 1:fileCnt
    dataSets(loadCnt) = load(FileArray{loadCnt,1}); %#ok<AGROW>
end

end


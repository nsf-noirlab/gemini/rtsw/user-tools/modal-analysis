function [fileName, pathName, ok] = bGetFileName(varargin)
%bGetFileNam Returns result from uigetfile
%   usage: [fileName, pathName, ok] = bGetFileName('fileFilter','title','on')
%     input (0-3 input variables):
%     'fileFilter' defaults to *.*. Only show these files.
%     'title' - display this text in the window.
%     'on' for multi-file select, 'off' for single file.
%
%     output:
%     standard fileName, pathName for selected file(s)
%     'ok' is a boolean indicating success or failure of file open
%
%   note: in calling routine, for full file use: 
%   f = fullfile(pathName,fileName);
% Bill McBride updated 28 June 2019

switch nargin
  case 0
    fileFilt = '';
    titl = '';
    multi = 'off';
  case 1
    fileFilt = varargin{1};
    titl = '';
    multi = 'off';
  case 2
    fileFilt = varargin{1};
    titl = varargin{2};
    multi = 'off';
  case 3
    fileFilt = varargin{1};
    titl = varargin{2};
    multi = varargin{3};
  otherwise
    error('Invalid number of inputs')
end

[fileName, pathName] = uigetfile(fileFilt,'asdf','MultiSelect',multi);

if isequal(fileName,0)
   disp('User selected Cancel');
   ok = 0;
else
   disp(['User selected ', fullfile(pathName,fileName)]);
   ok = 1;
end

end


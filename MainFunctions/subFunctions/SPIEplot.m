function SPIEplot(figName, varargin)
%SPIEplot Create figure and axis according to SPIE paper rules
%   Input: Figure handle, Axis handle, Figure name, file parts(cell array)
%   Output: Creates figure and axis

if nargin == 1
    np = {'','no',' name',' at all'};
elseif nargin == 2
    np = varargin{1};
else
    error('too many arguments');
end

figHandle = figure('Name',[figName,' ',np{2},' ',np{3}],...
    'Units','pixels',...
    'OuterPosition',[0,0,1950,1200]);

% Create axes
AxHandle = axes('Parent',figHandle);
hold(AxHandle,'on');

box(AxHandle,'on');
% Set the remaining axes properties
set(AxHandle,'FontName','Times New Roman',...
    'FontSize',60,...
    'LineWidth',3);
end

%% SPIE requirements:
% EPS, TIFF, PNG, PDF, or PS
% max width 3.3125 for single column, 6.75 for 2 column
% white background
% RGB or CMYK, no layers, LZW compression with .tiff files
% 0.5 point line minimum
% 300-600 pixels per inch
% min text size 8 pt - Times Arial or Symbol
% less than 2 - 3 MB per figure

% Implementation: set size to 3.25 inches by 2 inches with 600 ppi.
% so size is 1950 X 1200 pixels
% on screen, this is 19.5 inches X 13 inches, or 6X as big
% so minimum font size is 8 * 6 = 48
% minimum line size is 0.5 * 6 = 3
function [filtered] = HPfilter(inputDat, fs, Fpass)
%HPfilt High pass filter for column data
%   Filter the inputDat matrix with a 5th order IIR high pass filter
%   fs and passband are in Hz
%   order is the filter order

% 29 May 2016
% changed filter passband ripple from 0.1dB to 0.01dB to increase amplitude
% accuracy of discIntgrt() routine. Was a couple % off, now ~0.5%.

% High pass filter parameters
Dpass = 0.01;    % Passband Ripple (0.01dB)

% create 5th order high pass IIR  filter
HPfilt = designfilt('highpassiir',...
    'FilterOrder',5,...
    'PassbandFrequency',Fpass,...
    'PassbandRipple',Dpass,...
    'SampleRate',fs);

% filter all of the recorded data
filtered = filtfilt(HPfilt,inputDat);
end


% testProc

% provides data output for Grafana
% this data is the frequency, magnitude, and direction for each vibration
% peak
% for now, run "createSpectrogram.m" then run this on the data left in the
% base workspace

  %% Setup the plot here
  % hint: to increase time resolution, set frequency resolution lower
  freqMax = 250; % max frequency to plot
  fRes = 0.2; % desired frequency resolution (FFT bin size) in Hz.
  winSpec = 'HFT95'; % window for FFT
  % available windows are:
  % Rectang, Hanning, Hamming, BlackmanHarris, Nuttall3, Nuttall3a,
  % Nuttall3b, Nuttall4, Nuttall4a, Nuttall4b, Nuttall4c, HFT70, HFT95,
  % HFT90D, HFT116D, HFT144D, HFT169D, HFT196D, HFT223D, HFT248D

% number of fft points and overlap
% determined by desired frequency resolution: fRes rounded up to next power
% of 2 for effecient DFT processing
nfft = 2^nextpow2(dec.fs/fRes);
fResNew = dec.fs/nfft; % actual freq resolution
disp(['fft length = ',num2str(nfft),...
  ' bins, frequency resolution = ',num2str(fResNew), ' Hz'])
[win, ROV] = winGen(winSpec, nfft);
noverlap = (ROV/100) * nfft;
noverlap = cast(noverlap, 'int32');

%% Transform
% use the FFT of r (magnitude) to find peaks
% take FFT of x, y, z to get 3D vectors
disp('Fourier Transforms')
[OSS.xFFT,fvec,~] = winFFT(OSS.x,dec.fs,'LS',winSpec,'fres',fRes);
[OSS.yFFT,~,~] = winFFT(OSS.y,dec.fs,'LS',winSpec,'fres',fRes);
[OSS.zFFT,~,~] = winFFT(OSS.z,dec.fs,'LS',winSpec,'fres',fRes);

[CAS.xFFT,~,~] = winFFT(CAS.x,dec.fs,'LS',winSpec,'fres',fRes);
[CAS.yFFT,~,~] = winFFT(CAS.y,dec.fs,'LS',winSpec,'fres',fRes);
[CAS.zFFT,~,~] = winFFT(CAS.z,dec.fs,'LS',winSpec,'fres',fRes);

[MOS.xFFT,~,~] = winFFT(MOS.x,dec.fs,'LS',winSpec,'fres',fRes);
[MOS.yFFT,~,~] = winFFT(MOS.y,dec.fs,'LS',winSpec,'fres',fRes);
[MOS.zFFT,~,~] = winFFT(MOS.z,dec.fs,'LS',winSpec,'fres',fRes);

% find the magnitude of the vectors by converting to spherical coords and
% using the r component
[OSS.azFFT, OSS.elFFT, OSS.rFFT] = cart2sph(OSS.xFFT,OSS.yFFT,OSS.zFFT);
[MOS.azFFT, MOS.elFFT, MOS.rFFT] = cart2sph(MOS.xFFT,MOS.yFFT,MOS.zFFT);
[CAS.azFFT, CAS.elFFT, CAS.rFFT] = cart2sph(CAS.xFFT,CAS.yFFT,CAS.zFFT);

disp('finding peaks')
% remove noise floor (everything less than sMin
% optionally, use the prominance option (set sMin = 0)
%sMin = 3e-9;
sMin = 0;
pkProm = 1e-9;

datMin = OSS.rFFT;
datMin(OSS.rFFT<sMin) = 0;

% find peaks and plot peaks
figure(1)
plot(fvec,datMin)
[pks,locs] = findpeaks(datMin,fvec,'MinPeakProminence',pkProm);
findpeaks(datMin,fvec,'MinPeakProminence',pkProm)
title('Magnitude OSS')

% save frequencies, magnitudes, fvec indices
OSS.rPkFrq = locs;
OSS.rPkMag = pks;
OSS.pkIdx = ismember(fvec,locs);

% NEXT
figure(2)
datMin = CAS.rFFT;
datMin(CAS.rFFT<sMin) = 0;

% find peaks and plot peaks
plot(fvec,datMin)
[pks,locs] = findpeaks(datMin,fvec,'MinPeakProminence',pkProm);
findpeaks(datMin,fvec,'MinPeakProminence',pkProm)
title('Magnitude CAS')

% save frequencies and magnitudes
CAS.rPkFrq = locs;
CAS.rPkMag = pks;
CAS.pkIdx = ismember(fvec,locs);

% NEXT
figure(3)
datMin = MOS.rFFT;
datMin(MOS.rFFT<sMin) = 0;
title('Magnitude MOS')

% find peaks and plot peaks
plot(fvec,datMin)
[pks,locs] = findpeaks(datMin,fvec,'MinPeakProminence',pkProm);
findpeaks(datMin,fvec,'MinPeakProminence',pkProm)

% save frequencies and magnitudes
MOS.rPkFrq = locs;
MOS.rPkMag = pks;
MOS.pkIdx = ismember(fvec,locs);

% convert x,y,z into spherical coords

%% Output peaks and magnitudes
ossPeaks = horzcat(OSS.rPkFrq,OSS.rPkMag.*1e9,OSS.azFFT(OSS.pkIdx),OSS.elFFT(OSS.pkIdx));
casPeaks = horzcat(CAS.rPkFrq,CAS.rPkMag.*1e9,CAS.azFFT(CAS.pkIdx),CAS.elFFT(CAS.pkIdx));
mosPeaks = horzcat(MOS.rPkFrq,MOS.rPkMag.*1e9,MOS.azFFT(MOS.pkIdx),MOS.elFFT(MOS.pkIdx));





function [coudeIM, c] = coudeImageMotion(dispDat)
%[imotVect] = coudeImageMotion(dispDat)
%   Input the accelerometer displacement vectors
%   Return the complex image motion vector

% The coude rotator is treated as a simple rigid body able only to
% rotate about a point 9.6 meters below the center of its surface.

%% Optical Sensitivity 
% Coude sensitivity matrix in local coordinates (TN-0023 addendum)
% provided in an email from Rob.
xSens = [27.817 4.0789];
ySens = [0 -11.826];

% Image displacment in mm per milliradian in the reference frame of the
% focal plane (plate size is 1 mm = 1 arcsec)

% Rotation axis is x or y, image motion is [dY dZ] in arcsec (or mm) per
% milliradian rotation about a point RotPoint(m) below surface of coude
% rotator

%% Conversions
% Rotation point -z coord (m)
RotPoint = 9.6;

% angle in radians caused by 1mm displacement
RotConst = atan(0.001/RotPoint); % atan(1mm/d(m))

% angle in milli-radians per 1mm displacement
RotConst = RotConst * 1e3;

% Sensitivity matrix in arcsecs i.m. per mm displacement
cDxSens = RotConst * xSens;
cDySens = RotConst * ySens;

% Sensitivity matrix in arsecs i.m. per meter translation
cDxSens = cDxSens * 1e3; % arcsec/m
cDySens = cDySens * 1e3; % arcsec/m

%% SAT setup 2
% nine accelerometers mounted on permanent blocks under coude floor
rAcc = 7.47 % accelerometer placement (m) from center of rotation axis
zAcc = -0.84 % accelerometer placement (m) below floor surface

% accelerometer channels for coude
% position 1 - local coordinate y-axis
pYx = 1; % -x
pYy = 2; % -y
pYz = 3; % z
% position 2 - local coordinate x-axis
pXx = 5; % -x
pXy = 4; % y
pXz = 8; % z
% position 3 - local coordinate minus y-axis
mYx = 7; % x
mYy = 6; % y
mYz = 11; % z
% position 4 - local coordinate minus x-axis
mXx = 9; % x
mXy = 10; % -y

% calculate the six rigid body motions of the coude rotator
% note: all accelerometers have been calibrated to be in the plus axis
% orientation (positive displacement is positive x, y, or z)

% x translational
c.dx = 0.25*(dispDat(:,pYx) + dispDat(:,pXx) + dispDat(:,mYx) +...
    dispDat(:,mXx));

% y translational
c.dy = 0.25*(dispDat(:,pYy) + dispDat(:,pXy) + dispDat(:,mYy) +...
    dispDat(:,mXy));

% z translational
c.dz = 2./3.*(dispDat(:,pYz) + dispDat(:,pXz) + dispDat(:,mYz));

% rotation about z axis
c.rz = 0.25*(-dispDat(:,pYx) - dispDat(:,mXy) + dispDat(:,mYx) +...
    dispDat(:,pXy)); 

%{
%% SAT setup 1
% nine accelerometers mounted on permanent blocks under coude floor
rAcc = 7.47 % accelerometer placement (m) from center of rotation axis
zAcc = -0.84 % accelerometer placement (m) below floor surface

% accelerometer channels for coude
% position 1 - local coordinate y-axis
cX1 = 1; % -x
cX2 = 2; % -y
cX3 = 3; % z
% position 2 - local coordinate x-axis
cX4 = 4; % -x
cX5 = 5; % y
cX6 = 6; % z
% position 3 - local coordinate minus y-axis
cX7 = 7; % x
cX8 = 8; % y
cX9 = 9; % z

% calculate the six rigid body motions of the coude rotator
% note: all accelerometers have been calibrated to be in the plus axis
% orientation (positive displacement is positive x, y, or z)
c.dx = 0.5*(dispDat(:,cX1) + dispDat(:,cX7)); % x translational
c.dy = 0.5*(dispDat(:,cX2) + dispDat(:,cX8)); % y translational
c.dz = 0.5*(dispDat(:,cX3) + dispDat(:,cX9)); % z translational
% c.rz = 0.5*(dispDat(:,cX1) - dispDat(:,cX7)); % rotation about z axis
%}

%{
%% Coude FAT setup
% Only six accelerometers were used at the coude FAT.
% All future measurements were performed with the permanent mounting blocks
% using nine accelerometers

% accelerometer channels for coude
% position 1 - local coordinate y-axis
cX1 = 1; % -x
cX2 = 2; % -y
% position 2 - local coordinate x-axis
cX3 = 3; % y
cX4 = 4; % z
% position 3 - local coordinate minum y-axis
cX5 = 5; % -x
cX6 = 6; % z

% calculate the six rigid body motions of the coude rotator
% note: all accelerometers have been calibrated to be in the plus axis
% orientation (positive displacement is positive x, y, or z)
c.dx = 0.5*(dispDat(:,cX1) + dispDat(:,cX5)); % x translational
c.dy = 0.5*(dispDat(:,cX2) + dispDat(:,cX3)); % y translational
c.dz = 0.5*(dispDat(:,cX4) + dispDat(:,cX6)); % z translational
%}

%{
% to use individual accelerometers
c.dx = dispDat(:,cX1);
c.dy = dispDat(:,cX2);
c.dz = dispDat(:,cX3);
%}

% calculate M1 image motion in y and z axes; create 2-D vector
imotY = cDxSens(1).*c.dx;
imotZ = cDxSens(2).*c.dx + cDySens(2).*c.dy;

coudeIM = complex(imotY, imotZ); % y is real part, z is imaginary part
%imotMag = abs(iMotVect); % image motion magnitude

end

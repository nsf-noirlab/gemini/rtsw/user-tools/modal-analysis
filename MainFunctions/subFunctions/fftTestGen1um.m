% test script - from reference [1]
% Produce a signal with two sinusoidal signals with known amplitude and a
% noise background with known spectral density.
% The frequencies of the sinusoidal signals are not harmonically related to
% each other nor are the related to the sample frequency.
% The noise background is simulated by rounding the time-series data to
% interger multiples of ulsb.
% The noise floor is thus: U(noise) = ulsb/(sqrt(6*fs)).
% For fs = 10kHz and ulsb = 1mV, the noise is ~4.08uV/sqrt(Hz).

% The amplitude of 1um integrated twice gives 1e-6 * (2*pi*f)^2 = 
% 39.4784*f^2 = 3.948e-3 RMS.

fs = 1000;         % sample frequency (Hz)
f1 = 35;          % first sine wave frequency (Hz)
%amp1 = 3.948e-3*sqrt(2);    % 1 um pk
amp1 = 2e-3;    % 2 mm pk
%amp1 = 0.0001;

%f2 = 250.2157;     % second sine wave frequency (Hz)
%amp2 = 2;        % 1 uV pk
%ulsb = 1e-9;        % value of 1 LSB in Volts

L = 3e4;            % length of data set
T=1/fs;             % sample time

t = (0:L-1)'*T;     % time vector

% Change the signs around to make sure the phasing of the main program is
% ok. Change the amplitudes to make sure the vectors rotate correctly.

% OSS x
timeVars(:,1) = (amp1*0.5)*sin(2*pi*f1*t);
% OSS y
timeVars(:,2) = (amp1/0.5)*sin(2*pi*f1*t);
% OSS z
timeVars(:,3) = amp1*sin(2*pi*f1*t);
% MOS x
timeVars(:,4) = amp1*sin(2*pi*f1*t);
% MOS y
timeVars(:,5) = -amp1*sin(2*pi*f1*t);
% MOS z
timeVars(:,6) = amp1*sin(2*pi*f1*t);
% CAS x
timeVars(:,7) = amp1*sin(2*pi*f1*t);
% CAS y
timeVars(:,8) = amp1*sin(2*pi*f1*t);
% CAS z
timeVars(:,9) = -amp1*sin(2*pi*f1*t);

timeVars = timeVars + rand(size(timeVars)).* 1e-4;


%+ amp2*sin(2*pi*f2*t); % data vector
%u = floor(u/ulsb + 0.5)*ulsb; % round off each value to LSB value (noise)

clear f1 amp1 f2 amp2 ulsb L T

% plot 1 sec of data
figure(43);
plot(fs*t(1:1000,:),timeVars(1:1000,:));

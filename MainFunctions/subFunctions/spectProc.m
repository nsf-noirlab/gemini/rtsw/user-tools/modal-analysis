function [dec,datTim,OSS,MOS,CAS] = spectProc(sh,freqMax)
%spectProc Process accelerometer data. Called from createSpectrogram
%   Detailed explanation goes here

%% decimate data to speed up processing

% find number of accelerometers
numAccels = length(sh.accelArray{1});
% take vectors out of sh array so it can be deleted from memory
% decimate the data vectors to 2X freqMax to save memory
disp('decimating data vectors')
dec.factor = round(sh.fs/(freqMax*2));
if dec.factor <= 1
  dec.data = sh.data; % don't decimate if decFactor is 1
else
  for cnt = 1:numAccels
    dec.data(:,cnt) = decimate(sh.data(:,cnt),dec.factor);
  end
end
% time vector must be downsampled to match
dec.time = downsample(sh.time,dec.factor);
datTim = sh.datTim;
accelArray = sh.accelArray;
dec.fs = sh.fs/dec.factor;

%% Calibrate and orient accelerometer vectors

disp('calibrating accel data');

% scale the accelerometer data to units of m/s^2
acceleration = accelCalibrate(dec.data(:,1:numAccels), accelArray);

% align the polarity of the accelerometer data to the coordinate axes
acceleration = accelOrient(acceleration, accelArray);

disp('integrating and filtering accel data');

fFilt = 3; % High pass filter frequency in Hz
% create velocity and displacement vectors from acceleration vector
[~, displacement] = discIntgrt(acceleration,dec.fs,fFilt);
%displacement = detrend(displacement);

% put data into 3D vectors from the accelerometer data
disp('making vectors')

% put the data into the arrays, one for each location in the order of x,y,z
OSS.x = displacement(:,1);
OSS.y = displacement(:,2);
OSS.z = displacement(:,3);
MOS.x = displacement(:,4);
MOS.y = displacement(:,5);
MOS.z = displacement(:,6);
CAS.x = displacement(:,7);
CAS.y = displacement(:,8);
CAS.z = displacement(:,9);

disp('converting to spherical coords')
% now we need the the vectors in spherical coords
% I don't think we need the time vectors in spherical coords - except the r
% component
% [OSS.az, OSS.el, OSS.r] = cart2sph(OSS.x,OSS.y,OSS.z);
% [MOS.az, MOS.el, MOS.r] = cart2sph(MOS.x,MOS.y,MOS.z);
% [CAS.az, CAS.el, CAS.r] = cart2sph(CAS.x,CAS.y,CAS.z);
[~, ~, OSS.r] = cart2sph(OSS.x,OSS.y,OSS.z);
[~, ~, MOS.r] = cart2sph(MOS.x,MOS.y,MOS.z);
[~, ~, CAS.r] = cart2sph(CAS.x,CAS.y,CAS.z);

end


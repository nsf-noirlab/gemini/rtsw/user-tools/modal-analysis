Fs = 100;
T = 1/Fs;
L = 1000;
t = (0:L-1)*T;
y = sin(2*pi*10*t);

z = vertcat(zeros(450,1),ones(100,1),zeros(450,1))';

y = y .* z; % rect windowed data

w = winGen('Hanning',100)';
w=w';
n = vertcat(zeros(450,1),w,zeros(450,1))';
m = y .* n; %hanning windowed data

NFFT = 1000;
Y = fft(y.*10,NFFT)/L;
Z = fft(z,NFFT)/L;
H = fft(n.*40,NFFT)/L;
J = fft(m.*40,NFFT)/L;

f = Fs/2*linspace(0,1,NFFT/5+1);

%figure;
SPIEplot('sineFFT');
plot(f,2*abs(Y(1:NFFT/5+1)),'lineWidth',3)

%figure;
%SPIEplot('windowFFT');
%plot(f,abs(Z(1:NFFT/5+1)),'lineWidth',3)

%figure;
%SPIEplot('HanWinFFT');
%plot(f,abs(H(1:NFFT/5+1)),'lineWidth',3)

%figure;
SPIEplot('HanSineFFT');
plot(f,abs(J(1:NFFT/5+1)),'lineWidth',3)

%figure;
SPIEplot('sine');
plot(t,y,'lineWidth',3)

%figure;
%SPIEplot('rectWin');
%plot(t,z,'lineWidth',3)

%figure;
%SPIEplot('HanWin');
%plot(t,n,'lineWidth',3)

%figure;
SPIEplot('HanSine');
plot(t,m,'lineWidth',3)

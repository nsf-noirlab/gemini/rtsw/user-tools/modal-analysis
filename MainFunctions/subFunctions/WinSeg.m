function [dat,windw,segTime] = WinSeg(timeSeries,winType,wSpec,wInpt,ovlp,fs)
%WinSeg() Segment a time series and apply a window to each segment
%   Written by: Bill McBride
%   29 April, 2015
%
%   Usage:
%   [dat] = WinSeg(timeSeries,winType,wSpec,wInpt,ovlp,fs)
%
%   INPUTS:
%   timSeries   timeSeries vector to be weighted
%   winType     type of window (see winGen for available options)
%               (HFT95 if not provided)
%   wSpec       window specifier:
%               'len' - window length [points]
%               'max' - single window (length set to timeSeries length)
%               'num' - specify number of windows
%               'fres' - window length is determined by freq resolution
%   wInpt       input parameter:
%               if wSpec = len, window length
%               if wSpec = max, N/A
%               if wSpec = num, number of windowed segments
%               if wSpec = fres, frequency resolution in Hz
%   fres        frequency resolution [Hz] (ignored unless wSpec=fres)
%   ovlp        overlap [percentage] (leave blank for recommended overlap)
%   fs          sample frequency [Hz] (ignored unless wSpec=fres)
%
%   OUTPUTS:
%   dat         output structure with window weighted data segments in
%               columns
%   windw       a copy of the window used

%% input checks
tsLen = length(timeSeries);

if isempty(timeSeries)
    error('No timeSeries specified');
end

if isempty(winType)
    winType = 'HFT95';
end

if strcmp(wSpec,'len')
    winLen = wInpt;
elseif strcmp(wSpec,'max')
    winLen = tsLen;
elseif strcmp(wSpec,'num')
    winLen = 0;
elseif strcmp(wSpec,'fres')
    winLen = round(fs/wInpt); % length of window determined by desired fres
else
    error('wInpt invalid');
end

if strcmp(wSpec,'fres')
    timVec = (0:tsLen-1)/fs; % time vector
else
    timVec = 0:tsLen-1;
end
    
if winLen > tsLen
    error('Window too large for time series');
end

%% window calculations
% determine window overlap
if isempty(ovlp)
    [~,ovlp] = winGen(winType,winLen); % get recommended overlap
end

% calculate window length if based on number of windows
if strcmp(wSpec,'num')
    % (tsLen-winpt+1) subtracts the overlapping points
    % the first window is x long, the remaining are shorter by 1-ovlp%
    winLen = floor((tsLen-wInpt+1)/(1+(wInpt-1)*(1-ovlp*0.01)));
end

ovlpPts = round(winLen*(1-ovlp*0.01)); % num of data points in overlap
[windw,~] = winGen(winType,winLen); % get window

numWin = floor((tsLen-winLen)/ovlpPts+1); % calculate number of windows

%{
% plot of overlapping windows - warning: disable this when using more than
% about 50 windows or it takes forever and/or runs Matlab out of memory.
segPtr = 1;
winCtr = 1;
figure;
while segPtr-1 <= tsLen-winLen
    winPlotDat = zeros(tsLen,1);
    winPlotDat(segPtr:segPtr+winLen-1,1) = windw;
    plot(timVec, winPlotDat);
    hold on;
    segPtr = segPtr + ovlpPts;    
    winCtr = winCtr + 1;
end
hold off; % reset hold state
%}

%% process time series
dat = zeros(winLen,numWin); % preallocate data vectors
segPtr = 1; % pointer to the beginning of time series segment
winCtr = 1; % window counter
segTime = [];
while segPtr-1 <= tsLen-winLen
    segTime(winCtr,1) = (segPtr/fs);
    segTime(winCtr,2) = ((segPtr+winLen-1)/fs);
    % apply window to segment of input data
    segment = timeSeries(segPtr:segPtr+winLen-1,1).*windw;
    % troubleshooting aids:
    %timePlot = timVec(segPtr:segPtr+winLen-1); %%
    %disp(['window ',num2str(winCtr),' start ',num2str(segPtr),' end ',num2str(segPtr+winLen-1)]);
    %figure;
    %plot(timePlot,segment); %%

    dat(:,winCtr) = segment;
    % advance window counter
    winCtr = winCtr + 1;
    % advance pointer to beginning of next segment
    segPtr = segPtr + ovlpPts;
end
  %disp(['Number of windows: ',num2str(numWin)])
end


% Concatinate databuffer files

% This was the concatination file that was used for the Coude FAT tests,
% after that, the filename format was changed to 'databuffer001.mat' etc.

% 6/16/16
% added '-v7.3' switch

bufferCount = 1;
BfrName = 'databuffer1.mat'; % first buffer filename

% load each buffer one at a time and concatinate them into cumData and
% cumTime variables
while (exist(strcat(pwd,'/',BfrName),'file') == 2)
    
    disp(['loading databuffer', num2str(bufferCount),'.mat']);
    load(strcat(pwd,'/',BfrName), 'cumData', 'cumTime');
    
    if (bufferCount == 1)
        dataLast = cumData;
        timeLast = cumTime;
    else
        dataAll = vertcat(dataLast, cumData);
        timeAll = vertcat(timeLast, cumTime);
        dataLast = dataAll;
        timeLast = timeAll;
    end
    
    bufferCount = bufferCount + 1;
    BfrName = ['databuffer', num2str(bufferCount),'.mat'];
end

% add any remaining data in workspace to end of data and time variables
%{
if (exist('data', 'var') == 1 && exist('time', 'var') == 1)
    dataAll = vertcat(dataLast, data);
    timeAll = vercat(timeLast, time);
end
%}

% clean up workspace and save concatinated data file as dataCat.mat
data = dataAll; 
time = timeAll; 
clear BfrName bufferCount cumData cumTime dataLast timeLast;
clear dataAll timeAll;

disp('saving dataCat.mat')
save('dataCat', 'data', 'time','-v7.3')






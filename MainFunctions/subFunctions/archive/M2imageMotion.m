function [M2] = M2imageMotion(dispDat)
%[imotVect] = M1imageMotion(dispDat)
%   Input the accelerometer displacement vectors
%   Return the complex image motion vector

% M2 sensitivity matrix in local coordinates (TN-0023 table 3.2, table 3.4)
% motion in image plane y and z in arcsec/meter displacement or
% arcsec/radian rotation
M2dxSens = [0.425 -28.096]'*1e3; % arcsec/m
M2dySens = [27.458 0.417]'*1e3; % arcsec/m
M2dzSens = [4.146 0.063]'*1e3; % arcsec/m
M2rxSens = [-60.934 -0.926]'*1e3; % arcsec/rad
M2rySens = [0.904 -59.747]'*1e3; % arcsec/rad
M2rzSens = [0 0]'*1e3; % arcsec/rad

% accelerometer channels for M1
M2x11 = 7;
M2x12 = 8;
M2x21 = 9;
M2x22 = 10;
M2x31 = 11;
M2x32 = 12;

% accelerometer locations in global coords (parked at zenith pointing)
M2x1loc = [-1315.00 420.95 6739.12]'*1e-3; % from Paul
M2x2loc = [245.00 420.95 6739.12]'*1e-3;
M2x3loc = [-535.00 404.60 8474.04]'*1e-3;

% M1 center point in global coords (parked at zenith pointing)
% (SPEC-0049 rev G, page 4 for x and z)
% (calculated from M2xxloc for y)
M2cent = [-539.248 408.76 8032.860]*1e-3;

% lever arm length calcs from global coords
l1 = M2x3loc(3)-M2x2loc(3); % length of rotational lever arm about x axis
l2 = M2x2loc(1)-M2cent(1); % length of rotational lever arm about z axis

% %{
% decompose the six rigid body motions of M2
M2.dx = 0.5*(dispDat(:,M2x22) + dispDat(:,M2x32)); % M2 x translational
M2.dy = 0.5*(dispDat(:,M2x12) + dispDat(:,M2x31)); % M2 y translational
M2.dz = 0.5*(dispDat(:,M2x11) + dispDat(:,M2x21)); % M2 z translational
M2.rx = atan((dispDat(:,M2x12)-dispDat(:,M2x31))/l1); % M2 x rotational
M2.ry = atan((dispDat(:,M2x32)-dispDat(:,M2x22))/l1); % M2 y rotational
M2.rz = atan((dispDat(:,M2x12)-dispDat(:,M2x31))/l2); % M2 z rotational

%{
% for displaying individual accelerometers
M1.dx = dispDat(:,M1x11);
M1.dy = dispDat(:,M1x12);
M1.dz = dispDat(:,M1x21);
M1.rx = dispDat(:,M1x22);
M1.ry = dispDat(:,M1x31);
M1.rz = dispDat(:,M1x32);
%}

% calculate M1 image motion in y and z axes; create 2-D vector
imotY = M2dxSens(1).*M2.dx + M2dySens(1).*M2.dy + M2dzSens(1).*M2.dz +...
    M2rxSens(1).*M2.rx + M2rySens(1).*M2.ry + M2rzSens(1).*M2.rz;
imotZ = M2dxSens(2).*M2.dx + M2dySens(2).*M2.dy + M2dzSens(2).*M2.dz +...
    M2rxSens(2).*M2.rx + M2rySens(2).*M2.ry + M2rzSens(2).*M2.rz;

M2.imageMotion = complex(imotY, imotZ); % y is real part, z is imaginary part
%imotMag = abs(iMotVect); % image motion magnitude
end
%{
Notes on accelerometer placement and results:

The accelerometers were not placed properly to determine the rigid body
motions of M2 accurately; they should have been orthoginal to the axes, but
were instead in a triangle configuration.
The z-axis translations is accurate.
The x and y translations are accurate but will have some of the rotations
added to them, so they will be slightly overestimated.
x, y, and z rotations have some translation added.
%}

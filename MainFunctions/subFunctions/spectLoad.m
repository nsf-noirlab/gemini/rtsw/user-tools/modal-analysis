function [sh, fileName] = spectLoad()
%spectLoad File loader called by createSpectrogram3
%   Loads a file containing accelerometer data

% by William McBride
% 25 Nov 2020

%% Open and process file
[fileName, pathName, ok] = bGetFileName('wvib*.mat','select wvib file','off');
fullName = fullfile(pathName,fileName);
disp('Loading file...')
load(fullName,'sh'); 
if ok
  disp('File load complete')
else
  error('file load error')
end

% read accelArray and make a table of the info
% ...for future better display of accelerometer placement
accArr(:,1) = num2cell(sh.accelArray{1,1});
accArr(:,2) = num2cell(sh.accelArray{1,2});
accArr(:,3) = sh.accelArray{1,3};
accArr(:,4) = sh.accelArray{1,4};
accArr(:,5) = sh.accelArray{1,5};

disp(accArr)
end


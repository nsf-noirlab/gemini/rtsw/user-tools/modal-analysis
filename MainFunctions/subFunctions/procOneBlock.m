function [OSSfstr, MOSfstr, CASfstr] = procOneBlock(varNam)
% process one block of accelerometer data
% William McBride
% April 2020

% put the file name here
%fileNam = timeVars15;
dat = evalin('base',varNam);

% get the processed vectors
[OSSpks, OSSfstr, OSSls, win, f] = getVectors(1000,dat(:,1),...
  dat(:,2),dat(:,3),'OSS');
[MOSpks, MOSfstr, MOSls, ~, ~] = getVectors(1000,dat(:,4),...
  dat(:,5),dat(:,6),'MOS');
[CASpks, CASfstr, CASls, ~, ~] = getVectors(1000,dat(:,7),...
  dat(:,8),dat(:,9),'CAS');

% gather the peak frequencies from all 3 vectors
% due to noise, they may not be at exactly the same frequency
% the OSS likely has peaks that are not in the MOS and CAS vectors and vice
% versa

% all peaks
temp = union(union(CASpks.freqs,MOSpks.freqs),OSSpks.freqs);
numPks = length(temp);
allPks = zeros(numPks,10);
allPks(:,1) = temp;

% create a matrix with col 1 the frequency, and the x,y,z magnitudes in the
% remaining columns

for i = 1:numPks
  if ~isempty(find(OSSpks.freqs == allPks(i,1),1))
    allPks(i,2) = OSSpks.x(find(OSSpks.freqs == allPks(i,1),1));
    allPks(i,3) = OSSpks.y(find(OSSpks.freqs == allPks(i,1),1));
    allPks(i,4) = OSSpks.z(find(OSSpks.freqs == allPks(i,1),1));
  end
  if ~isempty(find(MOSpks.freqs == allPks(i,1),1))
    allPks(i,5) = MOSpks.x(find(MOSpks.freqs == allPks(i,1),1));
    allPks(i,6) = MOSpks.y(find(MOSpks.freqs == allPks(i,1),1));
    allPks(i,7) = MOSpks.z(find(MOSpks.freqs == allPks(i,1),1));
  end
  if ~isempty(find(CASpks.freqs == allPks(i,1),1))
    allPks(i,8) = CASpks.x(find(CASpks.freqs == allPks(i,1),1));
    allPks(i,9) = CASpks.y(find(CASpks.freqs == allPks(i,1),1));
    allPks(i,10) = CASpks.z(find(CASpks.freqs == allPks(i,1),1));
  end
  
end

% peaks in common
%combtube = intersect(CASpks.freqs, MOSpks.freqs);
%combAll = intersect(combtube,OSSpks.freqs);
%numPks = length(combAll);


%ossOnlyPks = setdiff(OSSpks.freqs, combAll);
%tubeOnlyPks = setdiff(combtube, combAll);

% this is needed for the beginning point of the 3D vectors
ref0 = zeros(numPks,1);
lims = 0.03;

for i = 1:numPks
  if allPks(i,1) > 3% temp to limit plots to freq range %%%%%%%%%%%%%%%%%%%%%%% fix this
  lims = max(allPks(i,2:10)); % get max amplitude for scaling
  % plot 3D vector
  figure;
  hAxq = gca;
  
  % locations for base of acceleration vectors
  OSS.x = 0;
  OSS.y = 0;
  OSS.z = lims*2;
  MOS.x = 0;
  MOS.y = lims;
  MOS.z = 0;
  CAS.x = -lims;
  CAS.y = 0;
  CAS.z = 0;
  
  % draw acceleration vectors
  quiver3(hAxq,OSS.x,OSS.y,OSS.z,allPks(i,2),allPks(i,3),allPks(i,4));
  hold on;
  quiver3(hAxq,MOS.x,MOS.y,MOS.z,allPks(i,5),allPks(i,6),allPks(i,7));
  quiver3(hAxq,CAS.x,CAS.y,CAS.z,allPks(i,8),allPks(i,9),allPks(i,10));
  
  % draw circle
  theta = linspace(-pi,pi,360);
  zc = zeros(1,length(theta));
  xc = cos(theta);
  yc = -sin(theta);
  xc = xc.*lims;
  yc = yc.*lims;
  plot3(hAxq,xc,yc,zc,'k');
  
  % draw OSS structure
  l.x1 = linspace(0,xc(45));
  l.y1 = linspace(0,yc(45));
  l.x2 = linspace(0,xc(135));
  l.y2 = linspace(0,yc(135));
  l.x3 = linspace(0,xc(225));
  l.y3 = linspace(0,yc(225));
  l.x4 = linspace(0,xc(315));
  l.y4 = linspace(0,yc(315));
  l.z = linspace(OSS.z,0);
  plot3(hAxq,l.x1,l.y1,l.z,'k');
  plot3(hAxq,l.x2,l.y2,l.z,'k');
  plot3(hAxq,l.x3,l.y3,l.z,'k');
  plot3(hAxq,l.x4,l.y4,l.z,'k');
  
  % set plot limits (scale to data)
  hAxq.XLim = [-lims*2, lims*2];
  hAxq.YLim = [-lims*2, lims*2];
  hAxq.ZLim = [-lims/2, lims*2.5];
  
  % label axes
  hAxq.FontSize = 12;
  hAxq.XLabel.String = 'X-axis (m)';
  hAxq.YLabel.String = 'Y-axis (m)';
  hAxq.ZLabel.String = 'Z-axis (m)';
  
  mag = num2str(round((lims * 1e3),2));
  % label plot with frequency and magnitude
  title(hAxq, [num2str(allPks(i,1)),' Hz, ',mag,' mm RMS']);
  
  hold off;
  end
  
  
end
end

% This is a script to process a bunch of accelerometer data
% The data file should contain variables like 'timeVars2', 'timeVars3', ...
% Each of these is 30 seconds of accelerometer data recorded by 9
% accelerometers on WIYN.
% The script loops through them and processes them to try and determine the
% modal frequencies and mode shapes being stimulated during the 30s period.
% Eventually, these should be presented so that they can be recorded and
% displayed in Grafana in some sort of way that makes sense.

% William McBride
% April 2020
% comments added and code cleaned-up: 17 Aug 2020

clear; % clear all variables from workspace
close all; % close all plots

% load file
load('quietTV2.mat'); % change this for a different file

c = who('timeVars*'); % get workspace variables 'timeVarsXX'
cntr = numel(c); % how many workspace variables?

% process each variable
for i = 1:cntr
  varName = c{i,1};
  disp(['processing ',varName])
  [OSSfstr, MOSfstr, CASfstr] = procOneBlock(varName);
end
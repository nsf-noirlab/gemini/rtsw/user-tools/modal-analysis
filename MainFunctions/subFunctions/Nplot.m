function Nplot(figName, varargin)
%Nplot Create figure and axis according to Bill's preferences
%   Input: Figure handle, Axis handle, Figure name, file parts(cell array)
%   Output: Creates figure and axis

if nargin == 1
    np = {'','no',' name',' at all'};
elseif nargin == 2
    np = varargin{1};
else
    error('too many arguments');
end

figHandle = figure('Name',[figName,' ',np{3},' ',np{4},' ',np{5},' ',np{4}],...
    'Units','inches',...
    'Position',[0 0 20 14]);

% Create axes
AxHandle = axes('Parent',figHandle,...
        'YGrid','on',...
        'YMinorGrid','on',...
        'XMinorTick','on',...
        'XMinorGrid','on',...
        'XGrid','on',...
        'XMinorTick','on',...
        'NextPlot', 'add',...
        'ColorOrderIndex', 1,...
        'Tag','1',...
        'FontSize',12);

% hold(AxHandle,'on');

box(AxHandle,'on');
% Set the remaining axes properties
set(AxHandle,'FontName','Times New Roman',...
    'FontSize',12,...
    'LineWidth',0.5);
end


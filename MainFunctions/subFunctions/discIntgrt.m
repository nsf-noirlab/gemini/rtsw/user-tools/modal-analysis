function [velDat,dispDat] = discIntgrt(accelDat,fs,Fstop)
%[velDat,dispDat] = discIntgrt(accelDat,fs,Fstop)
%   Perform disctrete-time integration of the input acceleration signal
%   Return velocity and displacement signals
%   The result of each integration is high-pass filtered with a stop
%   frequency of Fstop to remove the DC offset and drift

% if accelDat is in units of m/s^2
% velDat is in units of m/s
% dispDat is in units of m

% 22 Oct, 2018
% added feature if Fstop = 0, return with no filtering
% added detrend command to velDat and dispDat


dt = 1/fs;


%[rows, colms] = size(accelDat);
% taking the mean requires sorting out the rows and columns first

velDat = cumtrapz(accelDat);
velDat = velDat.*dt; % correct for frequency step size
%velDat = velDat - mean(velDat); % remove DC offset

if Fstop > 0
  velDat = HPfilter(velDat,fs,Fstop); % filter
end
velDat = detrend(velDat);

dispDat = cumtrapz(velDat);
dispDat = dispDat.*dt;
%dispDat = dispDat - mean(dispDat);

if Fstop > 0
  dispDat = HPfilter(dispDat,fs,Fstop);
end
dispDat = detrend(dispDat);

end


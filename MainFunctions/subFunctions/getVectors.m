function [pks, fstr, ls, win, f] = getVectors(Fs,x,y,z,name)
%getVectors(Fs,x,y,z,name) Plots the 3D vectors of acceleration data
%   getVectors() takes data from a group of 3 orthogonally mounted
%   accelerometers and identifies the peak frequencies and the 3D
%   displacement vector of each peak.

% William McBride
% April 2020

%{  
Configure the data and FFT parameters in the "%%setup" section below.

Input:    provide the sample frequency (Hz), and the x, y, and z axis
          acceleromter time vectors. "name" provides a name for the
          accelerometer location.

Outputs:  Structure named pks.xxx containing:
          .max      magnitude of linear spectrum of peaks
          .loc      index to peaks in vector
          .freqs    frequency of peaks
          .x        magnitude of x-accelerometer peak
          .y        "
          .z        "
          .num      number of peaks identified
          .xyPhs    cross-spectrum phase of xy peaks
          .xyMag    cross-spectrum magnitude of xy peaks
          .xzPhs    "
          .xzMag    "
          .yzPhs    "
          .yzMag    "

          Structure fstr.xxx containing the frequency, x, y, and z
            magnitudes fo the peaks.

          Structure ls.xxx containing the linear spectrum of the x, y, and
            z spectra and .xyz (sum of all three).

          Structure win.xxx
          .len      window length
          .type     window vector itself
          .ovlp     window overlap percentage (for pwelch window average
                    processing: see MATLAB "pwelch" function which is not
                    used, but winFFT uses a similar concept but provides
                    proper constants handling and allows the complex
                    spectra to be returned. The window averaging concept
                    is the same)

          Structure f.xxx for plot processing
          .min      minimum frequency to process / plot
          .max      maximum frequency to process / plot
          .res      frequency resolution of plots (bin width)
          .minIdx   index into the above structures for .min
          .maxIdx   index into the above structure for .max

By William McBride
2019, 2020
For work at WIYN telescope vibration and modal analysis

%}

%% setup
% note: on 26 March, the x and y coordinates were reversed to conform to
% the WIYN tube coordinate system.

plots = 'off'; % set to 'on' to display plots

% define window to use for analysis
% window length
win.len = 2000;
% window type
win.type = blackmanharris(win.len);
% window overlap in %
win.ovlp = 66;

% define frequencies of interest
% min freq to plot in Hz
f.min = 1;
% max freq to plot in Hz
f.max = 50;
% fft resolution (width of frequency bins) in Hz
f.res = 0.05;

% define peak identification parameters
% minimum height of identified peaks (m)
pkParams.minHt = 5e-4;
% minimum distance between peaks (Hz)
pkParams.minDist = 0.2;
% minimum prominance level (Hz) see help for findpeaks()
% numbers between 0.1 and 0.01 seem about right
pkParams.minProm = 0.05;

%% calculations
% convert from Hz to bins
pkParams.minDist = pkParams.minDist * f.res;
pkParams.minProm = pkParams.minProm * f.res;

% get linear spectrum of x, y, and z
[ls.x, freqs, ~] = winFFT(x,Fs,'LS','HFT95','fres',f.res);
[ls.y, ~, ~] = winFFT(y,Fs,'LS','HFT95','fres',f.res);
[ls.z, ~, ~] = winFFT(z,Fs,'LS','HFT95','fres',f.res);

% truncate frequency and fft vectors (necessary because of oversampling)
% get indices
f.minIdx = find(freqs==f.min);
f.maxIdx = find(freqs==f.max);
% truncate the vectors using the above indices
freqs = freqs(f.minIdx:f.maxIdx,1);
ls.x = ls.x(f.minIdx:f.maxIdx,1);
ls.y = ls.y(f.minIdx:f.maxIdx,1);
ls.z = ls.z(f.minIdx:f.maxIdx,1);

%% Find the resonances in the data

% add the spectra together
ls.xyz = ls.x + ls.y + ls.z;

% plot the x, y, z spectra plus the sum
% first, find peaks in linear spectrum (LS) to put  into the plot
[pks.max,pks.loc] = findpeaks(ls.xyz,'MinPeakHeight',pkParams.minHt,...
  'MinPeakDistance',pkParams.minDist,'MinPeakProminence',pkParams.minProm);
% pks.loc is the x index of the peak; pks.freq is the frequency of the peak
pks.freqs = freqs(pks.loc);

if strcmp(plots,'on')
  fig1 = figure;
  hFFT = semilogy(freqs,[ls.x,ls.y,ls.z,ls.xyz]);
  hold on;
  plot(pks.freqs,pks.max,'+')
  text(pks.freqs,pks.max,num2str((1:numel(pks.max))'))
  hold off;
  legend(hFFT,{'LS x', 'LS y', 'LS z','LS sum(x,y,z)'});
  grid on;
  title([name ' Vibration Linear Spectrum']);
end

% the x, y, z amplitudes of the peaks
pks.x = ls.x(pks.loc);
pks.y = ls.y(pks.loc);
pks.z = ls.z(pks.loc);

% total number of peaks found
pks.num = length(pks.max);

%% Convert the x,y,z vectors into one 3D vector

% measure the cross-spectrum between the x, y, and z vectors
[pxyc,freqs] = cpsd(x,y,win.type,win.ovlp,freqs,Fs);
[pxzc,~] = cpsd(x,z,win.type,win.ovlp,freqs,Fs);
[pyzc,~] = cpsd(y,z,win.type,win.ovlp,freqs,Fs);

%{
% use this if different resolution of cross-spectrum
% truncate the cross-spectrum vectors to the region of interest
% first, find the bin size
fcpsd.binSize = freqsCoh(2,1);
% now find the indices of the freqsCoh vector that match fmin and fmax
fcpsd.minIdx = find(abs(freqsCoh - f.min)<fcpsd.binSize/2);
fcpsd.maxIdx = find(abs(freqsCoh - f.max)<fcpsd.binSize/2);
if numel(fcpsd.minIdx) ~= 1 || numel(fcpsd.maxIdx) ~= 1
  errStr = ['something went wrong with indexing the freqsCoh vectors',...
    char(13),...
    'fcpsd.minIdx contains ', num2str(numel(fcpsd.minIdx)), ' elements',...
    char(13),...
    'fcpsd.maxIdx contains ', num2str(numel(fcpsd.maxIdx)), ' elements',...
    char(13),...
    'they should both contain only 1 element'];
  error(errStr)
end
% truncate all the vectors to only include fmin to fmax range
freqsCoh = freqsCoh(fcpsd.minIdx:fcpsd.maxIdx,:);
pxyc = pxyc(fcpsd.minIdx:fcpsd.maxIdx,:);
pxzc = pxzc(fcpsd.minIdx:fcpsd.maxIdx,:);
pyzc = pyzc(fcpsd.minIdx:fcpsd.maxIdx,:);
%}

% separate the real and complex parts of the cross-spectrum
pxy.mag = abs(pxyc);
%pxy.phs = imag(pxyc);
pxz.mag = abs(pxzc);
%pxz.phs = imag(pxzc);
pyz.mag = abs(pyzc);
%pyz.phs = imag(pyzc);
pxy.phs = angle(pxyc);
pxz.phs = angle(pxzc);
pyz.phs = angle(pyzc);

% plot the cross-spectrum mag and phase
if strcmp(plots,'on')
  figure(12);
  semilogy(freqs,pxy.mag,freqs,pxz.mag,freqs,pyz.mag)
  figure(13);
  plot(freqs,pxy.phs,freqs,pxz.phs,freqs,pyz.phs)
  hold on;
  plot(pks.freqs,0,'+');
  text(pks.freqs,pks.max,num2str((1:numel(pks.max))'))
  hold off;
end
%}

% cross-spectrum magnitude and phase of peaks
pks.xyPhs = pxy.phs(pks.loc);
pks.xyMag = pxy.mag(pks.loc);
pks.xzPhs = pxz.phs(pks.loc);
pks.xzMag = pxz.mag(pks.loc);
pks.yzPhs = pyz.phs(pks.loc);
pks.yzMag = pyz.mag(pks.loc);

% tricky part
% If the phase of the cross spectrum is zero or pi, that means that the
% accelerometers are moving together such that the combined vector is
% between them. If the slashes are accelerometers:
%       \ / moving up and down
% If the cross-spectrum is closer to pi/2, this means that the combined
% vector is not between them, but orthogonal to a vector between them.
%       \ / moving left and right
% So, arbitrarily choosing the z axis as the reference, inverting the
% magnitude of the x and y axes if the cross-spectrum is near pi/2, gives
% us a hemisphere of vectors.

% find the quadrant that the vector points in.
% the top hemisphere quadrants are:
% 1 +++
% 2 -++
% 3 --+
% 4 +-+
% the bottom hemisphere quadrants are:
% 5 ++-
% 6 -+-
% 7 ---
% 8 +--

% if in the bottom quadrant, switch to opposite top quadrant
% 5 => 3, 6 => 4, 7 => 1, 8 => 2 (the are indistinguishable anyhow)

for i = 1:pks.num
  % start out assuming they are in-phase
  xz = 1; yz = 1; xy = 1;
  
  % xz out of phase?
  if abs(pks.xzPhs(i)) > pi/2
    xz = 0;
  end
  % yz out of phase?
  if abs(pks.yzPhs(i)) > pi/2
    yz = 0;
  end
  % xy out of phase?
  if abs(pks.xyPhs(i)) > pi/2
    xy = 0;
  end  

  if xz==1 && yz==1 && xy==1
    % do nothing: quadrant 1
    %disp([num2str(i),' q1']);
  end
  if xz==0 && yz==1 && xy==0
    % invert x: quadrant 2
    %disp([num2str(i),' q2']);
    pks.x(i) = -pks.x(i);
  end
  if xz==0 && yz==0 && xy==1
    % invert x and y: quadrant 3
    %disp([num2str(i),' q3']);
    pks.x(i) = -pks.x(i);
    pks.y(i) = -pks.y(i);
  end
  if xz==1 && yz==0 && xy==0
    % invert y: quadrant 4
    %disp([num2str(i),' q4']);
    pks.y(i) = -pks.y(i);
  end
end
%{
% this may work - not sure
% invert the appropriate peaks
for i = 1:pks.num
  % if xz is out or phase, invert x
  if abs(pks.xzPhs(i)) > pi/4 && abs(pks.xzPhs(i)) < 3*pi/4
    pks.x(i) = -pks.x(i);
  end
  % if yz is out or phase, invert y
  if abs(pks.yzPhs(i)) > pi/4 && abs(pks.yzPhs(i)) < 3*pi/4
    pks.y(i) = -pks.y(i);
  end
end
%}

% peak frequency strings
fstr = cell(pks.num,4);
for i = 1:pks.num
  fstr(i,1) = {[num2str(round(pks.freqs(i),2)),' Hz ']};
  fstr(i,2) = {['x: ',num2str(round(pks.x(i).*1e3,2))]};
  fstr(i,3) = {['y: ',num2str(round(pks.y(i).*1e3,2))]};
  fstr(i,4) = {['z: ',num2str(round(pks.z(i).*1e3,2))]};
  %fstr(i,1) = st1,st2,st3,st4};
  assignin('base','fstr',fstr);
end

if strcmp(plots,'on')
  % this is needed for the beginning point of the 3D vectors
  ref0 = zeros(pks.num,1);
  % plot 3D vector
  figure;
  hAxq = gca;
  quiver3(hAxq,ref0,ref0,ref0,pks.x*1e6,pks.y*1e6,pks.z*1e6);
  hAxq.FontSize = 12;
  hAxq.XLabel.String = 'X-axis (\mum)';
  hAxq.YLabel.String = 'Y-axis (\mum)';
  hAxq.ZLabel.String = 'Z-axis (\mum)';
  hAxq.Title.String = [name ' Vibration Peak Amplitudes & Direction'];
  % set the limits so that they are the same as the maximum limit
  lims = max([hAxq.XLim,hAxq.YLim,hAxq.ZLim]);
  hAxq.XLim = [-lims, lims];
  hAxq.YLim = [-lims, lims];
  hAxq.ZLim = [-lims, lims];
  for i = 1:pks.num
    text(pks.x(i)*1e6,pks.y(i)*1e6,pks.z(i)*1e6,fstr(i,1),'FontSize',12,...
      'HorizontalAlignment','right','VerticalAlignment','top','Margin',1)
  end
end

%disp('done')
%{
figure(14);
polarplot(pks.xyPhs,0.9,'o',pks.xzPhs,0.9,'o',pks.yzPhs,0.9,'o')

figure(15);
histogram(abs([pks.xyPhs, pks.xzPhs, pks.yzPhs]),24)
%}

%% Coherence Analysis
%{
% obtain mag-squared coherence estimate using averaged coherence estimator
% (otherwise it will be 1 for all frequencies - not sure why but it is
% described in:
% https://www.mathworks.com/help/signal/ug/cross-spectrum-and-magnitude-squared-coherence.html

[coh.xy,~] = mscohere(x,y,win.type,win.ovlp,freqs,Fs);
[coh.xz,~] = mscohere(x,z,win.type,win.ovlp,freqs,Fs);
[coh.yz,~] = mscohere(y,z,win.type,win.ovlp,freqs,Fs);

clear x y z

%plot coherence
%%{
figure(1);
hCoh = gca;
plot(hCoh,freqs,coh.xy,'Color',cx(4));
hold on;
plot(hCoh,freqs,coh.xz,'Color',cx(5))
plot(hCoh,freqs,coh.yz,'Color',cx(6))
hold off;
title('Magnitude-Squared Coherence')
xlabel('Frequency (Hz)')
legend('x-y','x-z','y-z')
grid


% Create boolean vector for for the purpose of eliminating low-level
% non-coherent data points
coh.thres = 0.5; % coherence threshold
coh.xyMask = coh.xy > coh.thres;
coh.xzMask = coh.xz > coh.thres;
coh.yzMask = coh.yz > coh.thres;
%}

%% Calculate the 3D modal vectors
%{
% combine the x, y, and z vectors only if they are coherent with each other

% find the frequencies of the peaks and add into structure
pks.freqs = freqs(pks.max);

% put the linear spectrum amplitudes into structure
pks.lsx = ls.x(pks.all);
pks.lsy = ls.y(pks.all);
pks.lsz = ls.z(pks.all);

% pks.xpk contains 1 if there is a peak in x, or 0 if not, etc.
% these will be plotted even if the other axes are not coherent
pks.xpk = zeros(pks.num,1);
pks.ypk = zeros(pks.num,1);
pks.zpk = zeros(pks.num,1);

% find the indices of the peaks and make boolean of all x, y, and z peaks
% the same length as the number of peaks
[~,xIdx] = intersect(pks.all,pks.x);
[~,yIdx] = intersect(pks.all,pks.y);
[~,zIdx] = intersect(pks.all,pks.z);

% put the booleans into the structure
% these identify the peak values that triggered this frequency row to be
% included
pks.xpk(xIdx) = 1;
pks.ypk(yIdx) = 1;
pks.zpk(zIdx) = 1;

clear xIdx yIdx zIdx

% plot linear-spectrum and identified peaks
figure(4);
hPs = gca;
plot(hPs,freqs,ls.x,'Color',cx(1))
hold on;
plot(hPs,freqs,ls.y,'Color',cx(2))
plot(hPs,freqs,ls.z,'Color',cx(3))
plot(hPs,pks.freqs,pks.lsx.*pks.xpk,'v','Color',cx(1))
plot(hPs,pks.freqs,pks.lsy.*pks.ypk,'v','Color',cx(2))
plot(hPs,pks.freqs,pks.lsz.*pks.zpk,'v','Color',cx(3))
hold off;
hPs.YScale = 'log';
title('Linear Spectrum')
xlabel('Frequency (Hz)')
ylabel('Peak Amplitude (m)')
legend('Ax','Ay','Az')
grid on;

% put the coherence booleans into the structure
% these will be used to mask out non-coherent values (assumed to be noise)
pks.xyCoh = coh.xyMask(pks.all);
pks.xzCoh = coh.xzMask(pks.all);
pks.yzCoh = coh.yzMask(pks.all);

% plot coherence
figure(6);
hCs = gca;
plot(hCs,freqs,coh.xy.*coh.xyMask,'.')
hold on;
plot(hCs,freqs,coh.xz.*coh.xzMask,'.')
plot(hCs,freqs,coh.yz.*coh.yzMask,'.')
hold off;
%hCs.YScale = 'log';
title('Masked Coherence Amplitude')
xlabel('Frequency (Hz)')
ylabel('Amplitude')
legend('xyCoh','xzCoh','yzCoh')
grid on;
ylim([0.4 1.1]);

% if the peak is an identified peak, it is valid
% if the peak is in one of the cross spectrums, it is valid
% otherwise it is invalid
% for example: if a peak in the x-axis is in the xz or xy cross spectrum,
% it is valid
pks.xValid = pks.xpk | pks.xyCoh | pks.xzCoh;
pks.yValid = pks.ypk | pks.xyCoh | pks.yzCoh;
pks.zValid = pks.zpk | pks.xzCoh | pks.yzCoh;

% plot linear-spectrum and identified peaks
figure(8);
hLs = gca;
plot(hLs,freqs,ls.x,'Color',cx(1))
hold on;
plot(hLs,freqs,ls.y,'Color',cx(2))
plot(hLs,freqs,ls.z,'Color',cx(3))
% x y z primary peaks
plot(hLs,pks.freqs,pks.lsx.*pks.xpk,'s','Color',cx(1),'LineWidth',4)
plot(hLs,pks.freqs,pks.lsy.*pks.ypk,'s','Color',cx(2),'LineWidth',4)
plot(hLs,pks.freqs,pks.lsz.*pks.zpk,'s','Color',cx(3),'LineWidth',4)
% 1st coherence
plot(hLs,pks.freqs,pks.lsx.*pks.xyCoh,'^','Color',cx(1))
plot(hLs,pks.freqs,pks.lsy.*pks.xyCoh,'^','Color',cx(2))
plot(hLs,pks.freqs,pks.lsz.*pks.xzCoh,'^','Color',cx(3))
% 2nd coherence
plot(hLs,pks.freqs,pks.lsx.*pks.xzCoh,'v','Color',cx(1))
plot(hLs,pks.freqs,pks.lsy.*pks.yzCoh,'v','Color',cx(2))
plot(hLs,pks.freqs,pks.lsz.*pks.yzCoh,'v','Color',cx(3))
hold off;
hLs.YScale = 'log';
title('Linear Spectrum')
xlabel('Frequency (Hz)')
ylabel('Peak Amplitude (m)')
legend('Ax','Ay','Az')
grid on;

pks.v3d = [pks.xValid .* pks.lsx, pks.yValid .* pks.lsy, pks.zValid .* pks.lsz]; 

% this is needed for the beginning point of the 3D vectors
ref0 = zeros(pks.num,1);

% plot 3D vector
figure(7);
hvAxq = gca;
quiver3(hvAxq,ref0,ref0,ref0,pks.v3d(:,1),pks.v3d(:,2),pks.v3d(:,3));
hvAxq.FontSize = 12;
hvAxq.XLabel.String = 'X-axis (\mum)';
hvAxq.YLabel.String = 'Y-axis (\mum)';
hvAxq.ZLabel.String = 'Z-axis (\mum)';
hvAxq.Title.String = 'Vibration Peak Amplitudes & Direction';

% plot 3D vector
figure(5);
hAxq = gca;
quiver3(hAxq,ref0,ref0,ref0,pks.lsx,pks.lsy,pks.lsz);
hAxq.FontSize = 12;
hAxq.XLabel.String = 'X-axis (\mum)';
hAxq.YLabel.String = 'Y-axis (\mum)';
hAxq.ZLabel.String = 'Z-axis (\mum)';
hAxq.Title.String = 'Vibration Peak Amplitudes & Direction';

%{
% peak frequency strings
fstr = cell(pks.num,4);
for i = 1:pks.num
  fstr(i,1) = {[num2str(round(pkMat(i,2),1)),' Hz ']};
  fstr(i,2) = {['x: ',num2str(round(pkMat(i,3).*1e6))]};
  fstr(i,3) = {['y: ',num2str(round(pkMat(i,4).*1e6))]};
  fstr(i,4) = {['z: ',num2str(round(pkMat(i,5).*1e6))]};
  %fstr(i,1) = st1,st2,st3,st4};
  text(pkMat(i,6),pkMat(i,7),pkMat(i,8),fstr(i,:),'FontSize',12,'HorizontalAlignment',...
    'right','VerticalAlignment','top','Margin',1)
  assignin('base','fstr',fstr);
end

%}
%}

function c = cx(cNum)
% get the system default colors for use in plotting
% output is the triplet representing the default system color
if ~isnumeric(cNum)
  error('Wrong type: the input to function cx must be a number')
end

if cNum > 7 || cNum < 1
  error('Out of range: cNum in function cx must be between 1 and 7')
end

colors = get(groot,'defaultAxesColorOrder');
c = colors(cNum,:);
end

end


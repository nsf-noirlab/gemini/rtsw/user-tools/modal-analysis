% DataAcqViaGUI
% 
% This is a test file to experiment with putting a GUI on top of Bill's
% original DataAcqAll function

function DataAcqViaGUI()

  % we assume the graphics stuff is at the same directory level
  % and add it to the MATLAB path
  parentDir = cd;
  parentDir = fullfile(parentDir, '..');
  graphicsDir = fullfile(parentDir, 'graphicsFunctions');
  addpath(graphicsDir);
  
  top = figure('Visible', 'off');
  top.CloseRequestFcn = @closeReqCallback;
  configPanel = uipanel('Parent',top);
  configPanel.Position = [0.0, 0.25, 1.0, 0.75];
  configPanel.Title = 'Configuration acquisition';
  modeCombo = MComboBox(configPanel, {'accel', 'sweep','audio'});
  
  modeLabel = MLabel(configPanel, 'Mode');
  modeCombo.addLabel(modeLabel,'left');
  modeCombo.moveTo(200, 250);
  modeCombo.setCallback({@modeCallback, modeCombo});
  
  fsEntry = MTextField(configPanel, 2000);
  fsLabel = MLabel(configPanel, 'Sampling frequency (Hz)');
  fsEntry.addLabel(fsLabel, 'left');
  fsEntry.moveTo(200, 200);
  fsEntry.setCallback({@fsEntryCallback, fsEntry});
  
  fileEntry = MTextField(configPanel, 'AccelConfig.csv');
  fileLabel = MLabel(configPanel, 'Configuration file');
  fileEntry.addLabel(fileLabel, 'left');
  fileEntry.moveTo(200, 150);
  
  fileButton = MButton(configPanel, 'Change file...');
  fileButton.moveTo(320, 150);
  fileButton.setCallback({@fileButtonCallback, fileEntry});
  
  sweepModeLabel = MLabel(configPanel, 'Sweep mode parameters');
  sweepModeLabel.moveTo(100, 110);
  
  startDelayEntry = MTextField(configPanel, 10.0);
  startDelayLabel = MLabel(configPanel, 'Start delay (secs)');
  startDelayEntry.addLabel(startDelayLabel, 'left');
  startDelayEntry.moveTo(200, 80);
  startDelayEntry.setCallback({@sdEntryCallback, startDelayEntry});
  startDelayEntry.setEnabled(false);
  
  endDelayEntry = MTextField(configPanel, 10.0);
  endDelayLabel = MLabel(configPanel, 'End delay (secs)');
  endDelayEntry.addLabel(endDelayLabel, 'left');
  endDelayEntry.moveTo(200, 50);
  endDelayEntry.setCallback({@edEntryCallback, endDelayEntry});
  endDelayEntry.setEnabled(false);
  
  outTimeEntry = MTextField(configPanel, 120.0);
  outTimeLabel = MLabel(configPanel, 'Output time (secs)');
  outTimeEntry.addLabel(outTimeLabel, 'left');
  outTimeEntry.moveTo(200, 20);
  outTimeEntry.setCallback({@otEntryCallback, outTimeEntry});
  outTimeEntry.setEnabled(false);
  
  windSpeedValue = MLabel(configPanel, '0.0');
  windSpeedLabel = MLabel(configPanel, 'Wind speed (m/s)');
  windSpeedValue.moveTo(430, 110);
  windSpeedLabel.moveTo(300, 110);
  
  fontSize = 12;
  controlPanel = uipanel(top);
  controlPanel.Position = [0.0, 0.0, 1.0, 0.25];
  controlPanel.Title = 'Acquistion control';
  startButton = MButton(controlPanel, 'Start');
  startButton.setCallback(@startcb);
  startButton.setFontSize(fontSize);
  stopButton = MButton(controlPanel, 'Stop');
  stopButton.setCallback(@stopcb);
  stopButton.setFontSize(fontSize);
  exitButton = MButton(controlPanel, 'Exit');
  exitButton.setCallback(@closeReqCallback);
  exitButton.setFontSize(fontSize);
  startButton.moveTo(150, 50);
  stopButton.moveTo(250, 50);
  exitButton.moveTo(350, 50);
  
  % Add timer to regularly get the wind speed
  tw = timer('ExecutionMode', 'fixedRate', 'Period', 10.0,...
             'TimerFcn', @readWindSpeed, 'StartDelay', 2.0,...
             'ErrorFcn',@webReadErrorFcn);
  start(tw);
             
  top.Visible = 'on';
  
  %% GENERAL VARIABLE DECLARATIONS
  % INPUT VARIABLES
  % Buffers to accumulate data packets from s.DataAvailable events 
  cumData = [];
  cumTime = [];
  % Set an initial dummy value so that unavailble data is easy to spot
  currentWindSpeed = -99.0;

  % Numer of events to accumulate before saving the buffer to a file and
  % starting a new data accumulation.
  eventBufferSize = 30;

  % Counter for s.DataAvailable events
  dataEventCount = 0;

  % Counter for buffers that are filled when cumData and cumTime equal
  % eventBufferSize (saves file to disk and resets dataEventCount).
  bufferCount = 1;
  
  % get and display current date and time
  datTimNam = datestr(clock, 'yyyymmdd_HH_MM_SS');
  datTim = datestr(clock, 'mmmm dd, yyyy HH:MM:SS.FFF AM');
  
  accelArray = [];
  newFolder = [];
  
  numChannels = 0;
  extraCh = 0;
  
  pltSz = [1,2];
  colCntr = 1;
  
  FSCalib = 1.0;
  
  sessionRunning = false;
  
  % This is made a nested function so we have access to all the
  % graphics references like modeCombo, fsEntry etc.
  function startcb(~, ~)
    if (~sessionRunning) 
      mode = modeCombo.getSelectedItem(); 
      fs = str2double(fsEntry.getText()); % frequency for sweep output and sample frequency
      configFileName = fileEntry.getText();
      % Aquire accelerometer data while outputting sine sweep or other waveforms
      % using NI hardware.
      bufferCount = 1;
%{
Functions and files required:
AccelConfig() must be present and on the path.  AccelConfig requires two
text files: AccelSerials.txt and AcclConfig.txt.
 
Inputs:
none

Outputs:
saves the file "dataCat.mat" which contains the accelerometer and time
vectors.

May 1 2016
Upgraded from MATLAB R2014b to R2016a
The input command used previously to bail out of a background task
also stopped the data acquisition.  This was changed to a dialog button
instead (which is a lot nicer anyhow).

Functionality:
This function, when run, performs the setup of the NI hardware and begins
accumulating data as a foreground process.  The data is accumulated until
the output buffer runs out of data.

The NI hardware outputs the aquired data in packets at a rate set by
s.Rate.  The hardware default for s.Rate is 1000 samples per second but
this can be changed by setting a new value to s.Rate.
The packets are accumulated in the cumData and cumTime buffers and saved as
data and time in the function workspace.
Individual packets are ready when the s.DataAvailable event occurs and this
triggers the reading of the packet, updating the plot, and appending the
packet to the existing data in the cumData and cumTime buffers.

The size of the packets are determined by the
s.NotifyWhenDataAvailableExceeds property with a default value
of 1/10 sec.  In other words, the s.DataAvailable event occurs at a time
rate determined by s.NotifyWhenDataAvailableExceeds property.
 
If the data accumulation runs for too long, a Matlab memory overflow
occurs.
For this reason,   The cumData and cumTime buffer sizes are limited to a
value set by eventBufferSize.  eventBufferSize serves as an upper limit to
the number of s.DataAvailable events that are accumulated in the cumData
and cumTime buffers.  When the number of events match the value in
eventBufferSize, the buffers are saved to a file named 'dataBufferX' where
X starts at 1 and increments for each additional file saved.

For reference:
Running with 16 channels at a sample rate of 1000 samples per second, the
overflow occured after 614888 samples were aquired.  This was about 10.25
minutes of data accumulation.  At this point, Matlab reported an "out of
memory error."  This is not consistant - it depends on the mood that
Windows is in.

A good value might be 5 minutes (300s).
So with 16 channels:
fs=1000
s.NotifyWhenDataAvailableExceeds 1000 (1 sec)
eventBufferSize is set to 3000 (300sec / 1/10sec = 3000 events). 
%}

% Program start **********

%% BUG WORKAROUND
% These two lines are a workaround for a bug in the Matlab code that hard
% wires the PXIe-6363 card to the 10MHz clock (which is invalid).  The bug
% should be fixed in the release after 2014a

% two lines below commented out to try the fix documented in
% www.mathworks.com/support/bugreports/1030926
%daq.reset
%daq.HardwareInfo.getInstance('DisableReferenceClockSynchronization',true);

%% OUTPUT SETTINGS
% Select Mode:

% accel mode is used when recording an unknown length of data - data
% acquisition stops when the user hits 'enter'.  The other modes output a
% file and record data for a period of time defined by the startDelay,
% EndDelay, and outTime settings.
%mode = 'sweep'; % generate a sine sweep using a shaker and accelerometers
    % the force sensor is on channel 16
%mode = 'audio'; % play an audio file using a shaker and accelerometers
    % the force sensor is on channel 16
    
% I think I should change this to:
    % background / foreground
    % read accelconfig file for channel assignments of force sensor and
    % accelerometers, force sensors, or whatever - get units etc.

  % Sweep Settings
  % startDelay is the time in seconds of data acq before output starts e.g.
  % 10
  % endDelay is the time in seconds of data acq after output ends (must be
  % >0) e.g. 10
  % outTime is time in seconds to output data e.e. 120
  startDelay = str2double(startDelayEntry.getText());
  endDelay = str2double(endDelayEntry.getText());
  outTime = str2double(outTimeEntry.getText());


  if strcmp(mode,'audio')
    outputData = load('CrazyTrain');
    outSampRate = outputData.fs;
    outputData = outputData.data(:,1) + outputData.data(:,2);
    extraCh = 2;
  end

  % inputs for the the chirp vector
  if strcmp(mode,'sweep')
    outSampRate = fs;
    freqStart = 3; % beginning frequency of chirp in Hz
    freqEnd = 50; % ending frequency of chirp in Hz
    outputData = ChirpGen(outSampRate, startDelay, endDelay, outTime,...
        freqStart, freqEnd);
    extraCh = 2;
  end

  if strcmp(mode,'accel')
    outSampRate = fs;
    extraCh = 0;
  end

  % Force sensor
  % Enter the calibration value for the force sensor in mV/N
  %CVal = 2.318; % for 500 lb force sensor
  CVal = 11.241; % for 100 lb force sensor
  FSCalib = 1000/CVal; % convert to N/V
  % (this is a Model 208C03 ICP force sensor s/n LW41328)


  %% SETUP Accelerometer table
  % Call accelArray() to get configuration of accelerometers
  fname = strcat('../configFiles/', configFileName);
  accelArray = AccelConfig(fname);
  % save('accelArray', 'accelArray');

  % Run script to generate accelArray{} table
  ActiveCh = accelArray{1,1};

  %% CREATE DIRECTORY & FILENAME

  % get and display current date and time
  datTimNam = datestr(clock, 'yyyymmdd_HH_MM_SS');
  datTim = datestr(clock, 'mmmm dd, yyyy HH:MM:SS.FFF AM');

  % make directory that includes date and time
  newFolder = ['SHKR_',datTimNam];
  [dirStat,dirErrMsg,dirErrMsgid] = mkdir(newFolder);
  if ~dirStat
    disp(dirErrMsg);
    disp(dirErrMsgid);
    return;
  end

  %% CLEAR ANY OLD DATA BUFFERS IN THE CURRENT DIRECTORY
  delete('dataBuffer*.mat');

  %% CREATE PLOTS
  % Set up plot for data capture channels
  numChannels = length(accelArray{1,1});
  switch numChannels + extraCh
    case 2
        pltSz = [1 ,2];
    case 3
        pltSz = [2 ,2];
    case 4
        pltSz = [2 ,2];
    case 5
        pltSz = [2 ,3];
    case 6
        pltSz = [2 ,3];
    case 7
        pltSz = [3 ,3];
    case 8
        pltSz = [3 ,3];
    case 9
        pltSz = [3 ,3];
    case 10
        pltSz = [4 ,3];
    case 11
        pltSz = [4 ,3];
    case 12
        pltSz = [4 ,3];
    case 13
        pltSz = [5 ,3];
    case 14
        pltSz = [5 ,3];
    case 15
        pltSz = [5 ,3];
    case 16
        pltSz = [6 ,3];
    case 17
        pltSz = [6 ,3];
    case 18
        pltSz = [6 ,3];
    otherwise
        disp('Error: number of channels invalid');
  end

  % plot line colors
  colors = get(groot,'defaultAxesColorOrder');
  hAccelFig = figure('Position',[0 0 1024 768]); %#ok<NASGU>
  hAccelFig.CloseRequestFcn = @figCloseReqCallback;

  hAx = zeros(numChannels+extraCh,1);
  for i = 1:numChannels+extraCh
    hAx(i) = subplot(pltSz(1),pltSz(2),i,...
        'NextPlot', 'add');
  end
  for i = 1:numChannels  
    title(hAx(i),accelArray{1,3}(i,1));
    ylabel(hAx(i),strcat(accelArray{1,5}(i,1),...
    accelArray{1,4}(i,1),' m/s^2'));
  %accelArray{1,4}(i,1),' N'));

  end

  if ~strcmp(mode,'accel')

    FSplotNum=numChannels + 1; % force sensor plot number
    DrPlotNum = numChannels + 2; % drive plot number
    title(hAx(FSplotNum),'Force Sensor');
    ylabel(hAx(FSplotNum),'Newtons');
    set(hAx(FSplotNum),'YLim',[-450 450]);
    title(hAx(DrPlotNum),'Shaker Drive');
    ylabel(hAx(DrPlotNum),'Volts');
    set(hAx(DrPlotNum),'YLim',[-1 1]);
  end
  set(hAx,'FontSize', 10); % for all plots

  %% HARDWARE SETUP
  % Create the data acquisition session
  s = daq.createSession('ni');
  disp('DAQ session created');
  sessionRunning = true;

  % Setup DAQ accelerometer channels
  orig_state = warning; % save warning state
  warning('off','all') % disable all warnings(DAQ spits lots of useless ones)
  for i=ActiveCh
    s.addAnalogInputChannel('PXI1Slot2', ActiveCh, 'IEPE');
  end
  if ~strcmp(mode,'accel')
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    s.addAnalogInputChannel('PXI1Slot2', 'ai15', 'IEPE'); % force sensor chnl
      %s.addAnalogInputChannel('PXI1Slot2', 'ai15', 'IEPE'); % force sensor chnl
  end
  warning(orig_state); % restore warning state
  s %#ok<NOPRT>

  if ~strcmp(mode,'accel')

    % Setup Shaker Drive output channel
    s.addAnalogOutputChannel('PXI1Slot3','ao0', 'Voltage');
    % Setup Shaker Drive monitor input channel
    driveIn = s.addAnalogInputChannel('PXI1Slot3','ai1', 'Voltage');
    driveIn.TerminalConfig = 'SingleEnded';

    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % queue up the first chunk in the output data buffer
    %outDataBufSize = 1000;
    %s.queueOutputData(outputData(1:outDataBufSize,1));
    s.queueOutputData(outputData(1:end,1));
    %outputData = outputData(outDataBufSize+1:end,1); % delete queued data
  end

  % Set property values
  if strcmp(mode,'accel')
    s.IsContinuous = true; % run continuously until stopped by user
  else
    s.IsContinuous = false; % run continuously until stopped by user
  end

  s.Rate = outSampRate; % set the sample rate in samples/second
  s.NotifyWhenDataAvailableExceeds = 1000; % set the packet size
  s.NotifyWhenScansQueuedBelow = 3000; % set threshold to queue new scans

  %% --------------------LISTENERS--------------------
      % Set up listeners to create events
      hError = s.addlistener('ErrorOccurred', @ErrorCallback);

      hGetOutData = s.addlistener('DataRequired', @QueOutputData);
      hPlot = s.addlistener('DataAvailable', @dataAccumulate);
      sData = struct('session',s,'hError',hError,'hPlot',hPlot,'hGetOutData',hGetOutData);
      guidata(top, sData);
    else
        h = msgbox('A session is already running', 'Session Active');
    end
  %% --------------------Callback Functions --------------------------
  
  % Prevent close of accelerometer plots whilst a session is running
      function figCloseReqCallback(~,~)
        if (~sessionRunning)
           delete(gcf);
        else
           h = msgbox('This figure must stay open while a session is running',...
               'Session Running');
        end
      end
      
  % Callback to handle errors
  function ErrorCallback(src, event) %#ok<INUSL>
    disp('ERROR in hardware session: ');
    disp(event.Error.getReport());
  end

  % unused callback to queue more output data
  function QueOutputData(src, event) %#ok<INUSD>
    disp('data output buffer exhausted ')
  end

  % Callback to queue data in FIFO for output
  %{
  function QueOutputData(src, event) %#ok<INUSD>
    if length(outputData) <= 1
        disp(length(outputData));
        finished = true;
    elseif length(outputData) <= outDataBufSize
        disp(length(outputData));
        s.queueOutputData(outputData(1:end,1));
        outputData = [];
    else
        disp(length(outputData));
        s.queueOutputData(outputData(1:outDataBufSize,1));
        outputData = outputData(outDataBufSize+1:end,1);
    end
  end 
  %}

  % Plotting and buffer saving callback function
  function dataAccumulate(src,event) %#ok<INUSL>
    dataEventCount = dataEventCount + 1; % count packets
    cumData = vertcat(cumData, event.Data); % accumulate packets
    cumTime = vertcat(cumTime, event.TimeStamps);

    % Plot the data in each subplot
    colCntr = 1; % counter for system default colors 1-7
    for index = 1:numChannels
%        plot(hAx(index), event.TimeStamps, event.Data(:,index),...
%            'Color', colors(colCntr,:));
        plot(hAx(index), event.TimeStamps, event.Data(:,index),...
            'Color', colors(colCntr,:));            
            %88.960 .* event.Data(:,index),...% remove 88.96 after FS tests!!!!!!!!!!!!!!

        colCntr = colCntr+1;
        if colCntr > 7
            colCntr = 1;
        end
    end
    if ~strcmp(mode,'accel')

        % Plot the force sensor signal
        cla(hAx(FSplotNum)); % remove this to plot same time scale as accels
        FSplotDat = event.Data .* FSCalib;
        plot(hAx(FSplotNum), event.TimeStamps, FSplotDat(:,FSplotNum),...
                'Color', colors(colCntr,:));
            colCntr = colCntr+1;
            if colCntr > 7
                colCntr = 1;
            end
        % Plot the shaker input drive signal    
        cla(hAx(DrPlotNum)); % remove this to plot same time scale as accels
        plot(hAx(DrPlotNum), event.TimeStamps, event.Data(:,DrPlotNum),...
                'Color', colors(colCntr,:));
    end
    
    % When buffer is full, save it, clear it, and keep accumulating
    if dataEventCount >= eventBufferSize %Buffer is full
        % number filename
        fileName = ['dataBuffer',num2str(bufferCount,'%04u')]; 
        disp(['saving file ',fileName]);
        speed = windSpeedValue.getText();
        if (strcmp(speed, 'Unavailable'))
            currentWindSpeed = -99.0;
        else
            currentWindSpeed = str2double(speed);
        end
        save(fileName, 'cumData', 'cumTime','currentWindSpeed');
        disp([fileName ' has been saved.']);
        
        bufferCount = bufferCount + 1; %Increment buffer counter
        dataEventCount = 0; % reset data event counter
        cumData = []; % empty buffers and continue
        cumTime = [];

        for index = 1:numChannels+extraCh
            cla(hAx(index));
        end
    end
  end
    
  %% ACQUISITION
  % Start the acquisition
  if ~strcmp(mode,'accel')
    top.Pointer = 'watch';
    s.startForeground();
    top.Pointer = 'arrow';
    shutdown(sData, outputData);
  else
    s.startBackground();
  end

  disp('DAQ task started in background');

  end

    function shutdown(sData, outputData)
      mode = modeCombo.getSelectedItem();
      fs = str2double(fsEntry.getText());
      
      s = sData.session;
      hPlot = sData.hPlot;
      hError = sData.hError;
      hGetOutData = sData.hGetOutData;
      
      %% STOP SESSION AND RELEASE HARDWARE
      s.stop(); % stop the session
      delete(hPlot); % delete the listener events
      delete(hError);
      delete(hGetOutData);
      s.release(); % release the hardware
      delete(s); % delete the session object
      clear s; % delete the session from memory
      guidata(top, []);
      disp('DAQ hardware released');
      sessionRunning = false;
      
      %% SAVE AND CONCATINATE THE DATA
      % Aquisition stopped so save the last data and time variables 
      save('dataBufferLast', 'cumData', 'cumTime', 'currentWindSpeed');
      
      % Concatinate databuffer files
      bufferCount = 1;
      BfrName = 'dataBuffer0001.mat'; % first buffer filename
      dataLast = [];
      timeLast = [];
      windSpeedLast = [];
      
      % load each buffer one at a time and concatinate them into cumData and
      % cumTime variables
      while (exist(strcat(pwd,'/',BfrName),'file') == 2)
    
        disp(['loading dataBuffer', num2str(bufferCount,'%04u'),'.mat']);
        load(strcat(pwd,'/',BfrName), 'cumData', 'cumTime', 'currentWindSpeed');
    
        if (bufferCount == 1)
          dataLast = cumData;
          timeLast = cumTime;
          windSpeedLast = currentWindSpeed;
        else
          dataAll = vertcat(dataLast, cumData);
          timeAll = vertcat(timeLast, cumTime);
          windSpeedAll = vertcat(windSpeedLast, currentWindSpeed);
          dataLast = dataAll;
          timeLast = timeAll;
          windSpeedLast = windSpeedAll;
        end
    
        bufferCount = bufferCount + 1;
        BfrName = ['dataBuffer', num2str(bufferCount,'%04u'),'.mat'];
      end
      
      % add remaining partial data file if it exists
      if (exist(strcat(pwd,'/','dataBufferLast.mat'),'file') == 2)
        load(strcat(pwd,'/','dataBufferLast.mat'), 'cumData', 'cumTime', 'currentWindSpeed');
        dataAll = vertcat(dataLast, cumData);
        timeAll = vertcat(timeLast, cumTime); 
        windSpeedAll = vertcat(windSpeedLast, currentWindSpeed);
      end
      
      % create structure and save with data and time variables
      sh.data = dataAll;
      sh.time = timeAll;
      sh.windSpeed = windSpeedAll;
      sh.datTim = datTim;
      sh.accelArray = accelArray;
      if ~strcmp(mode,'accel')
        sh.source = outputData;
      sh.FSCalib = FSCalib;
      end
      sh.fs = fs;
      sh.notes = ['The first columns of sh.data are the accelerometer data ',...
      '(see sh.accelArray)',...
      'The next column is the force sensor signal in raw units.',...
      '(multiply by sh.FSCalib to obtain units of Newtons)',...
      'The last column is the recorded drive signal in Volts',...
      'sh.time is the time vector.',...
      'sh.source is the Matlab generated drive signal.',...
      'sh.fs is the sample frequency.',...
      'sh.datTim is the date and time.',...
      'sh.windSpeed is the wind speed in m/s.',...
      'the filename is SHKR_ followed by the date as YYYYMMDD_hh_mm_ss.' ]; %#ok<STRNU>

      save([newFolder,'/',newFolder],'sh','-v7.3'); % force v7.3 for >2GB
      disp(['File saved to ',newFolder]);
      delete('dataBuffer*.mat')
      
      %% To display fft of each channel
      %{
      load([newFolder,'/',newFolder],'sh');

      hFFTFig = figure('Position',[0 0 1024 768]); %#ok<NASGU>

      hFFTAx = zeros(numChannels+extraCh,1);
      for i = 1:numChannels+extraCh
        hFFTAx(i) = subplot(pltSz(1),pltSz(2),i,...
          'NextPlot', 'add');
      end
      for i = 1:numChannels  
        title(hFFTAx(i),accelArray{1,3}(i,1));
        ylabel(hFFTAx(i),strcat(accelArray{1,5}(i,1),...
          accelArray{1,4}(i,1),' N'));
      end
      set(hFFTAx,'FontSize', 10); % for all plots

      colors = get(groot,'defaultAxesColorOrder');
      for i = 1:numChannels
        [plotData,fvec,~] = winFFT3(sh.data(:,i),sh.fs,1,'','PS');
        fvecLen = floor(length(fvec)/5); %plot only to max freq /2
        semilogy(hFFTAx(i), fvec(1:fvecLen,1), plotData(1:fvecLen,1),...
            'Color', colors(colCntr,:));                       
        colCntr = colCntr+1;
        if colCntr > 7
            colCntr = 1;
        end
      end
      
      %for plotCount = 1:numChannels;
      %    [plotData,fvec,s] = winFFT2(sh.data(:,plotCount),sh.fs,1,'','PS');
      %    semilogy(fvec,plotData);
      %    hold on;
      %end
      %}
    end

    function closeReqCallback(source, ~)
        data = guidata(source);
        if (isempty(data))
           delete(gcf);
           stop(tw);
           delete(tw);
        else 
          if (~data.session.IsDone)
            selection = questdlg('Acquisition is running. Do you really want to close?',...
                                 'Close application', 'Yes', 'No', 'No');
            switch selection
                case 'Yes'
                  shutdown(data, []);
                  stop(tw);
                  delete(tw);
                  delete(gcf);
                case 'No'
                return
            end
          else
            delete(gcf);
            stop(tw);
            delete(tw);
          end
        end
    end
  
    function webReadErrorFcn(~, ~)
        disp('Error accessing IfA weather data. Wind speed not available');
        windSpeedValue.setText('Unavailable');
        windSpeedValue.setEnabled(false);
    end

    function readWindSpeed(~, ~)
      ifa = 'https://ps1wiki.ifa.hawaii.edu/cgi-bin/10SecWeather?weather';
      rawdata = webread(ifa);
  
      % The date is the first part of the rawdata separated by whitespace
      % from the rest
      strs = split(rawdata);
      date = strs(1);
  
      % The remainder of the data is comma separated so split it up and
      % extract the parts that are relevant
      data = split(strs(2), ',');
  
      % The array elements are:
      % 1 - the current UTC
      % 2 - temperature (deg C)
      % 3 - Atmospheric presure (mbar)
      % 4 - Relative humidity
      % 5 - wind speed (m/s)
      % 6 - wind direction (degs)
      % 7 - Visibility (m)
      % 8 - Co2 (unavailable)
      % 9 - insolation (Langley/hr)
      % 10 - v wind speed (?) 
      % 11 - rainfall (unavailble)
      % 12 - dew point (deg F)
      windSpeedValue.setText(data(5));
    end

  function modeCallback(~, ~, mode)
   selection = mode.getSelectedItem();
   if (strcmp(selection, 'sweep'))
       startDelayEntry.setEnabled(true);
       endDelayEntry.setEnabled(true);
       outTimeEntry.setEnabled(true);
   else
       startDelayEntry.setEnabled(false);
       endDelayEntry.setEnabled(false);
       outTimeEntry.setEnabled(false);
   end
  end

  function stopcb(source, ~)
    mode = modeCombo.getSelectedItem();
    data = guidata(source);
    % If a session hasn't been started or
    % we are not running in the background then do nothing
    if (~isempty(data) && strcmp(mode, 'accel'))
      shutdown(data,[]);
    end
  end
end
%% LOCAL FUNCTIONS
function [ chirpData, t ] = ChirpGen( fs, stDelay, endDelay, len, Fst, Fend )
%ChirpGen Generates chirp vector
%   Generate a zero padded chirp vector at frequency fs
%   fs = sample frequency in Hz
%   stDelay = zero pad preceding chirp in seconds
%   endDelay = zero pad following chirp in seconds
%   len = length of active chirp in seconds
%   Fst = start frequency of chirp in Hz
%   Fend = end frequency of chipr in Hz
%   swpShape = frequency sweep:
%       linear f(t) = f0+bt where b = (f1-f0)/t1
%       quadratic f(t) = f0 + bt^2 where b = (f1-f0)/t1
%       logarithmic f(t) = f0 * b^2 where b = (f1/f0)^(1/t1)

sPer = 1/fs; % calculate sample period
stPad = zeros(fs * stDelay, 1); % create zero pad start vector
endPad = zeros(fs * endDelay, 1); % create zero pad end vector
t1 = 0:sPer:len-sPer; % chirp time vector
swpShape = 'logarithmic';

% generate unpadded chirp vector
sinChirp = chirp(t1, Fst, len-sPer, Fend, swpShape, -90)';

% generate padded chirp and time vectors
chirpData = vertcat(stPad, sinChirp, endPad) .* 0.5; % padded chirp vector
t = 0:sPer:stDelay+endDelay+len-sPer; % chirp time vector

% uncomment to plot complete chirp waveform
% plot(t, chirpData);

end
%% Local function to read acceleration .csv file
function [accelArray] = AccelConfig(filename)
%{
Read configuration file and create cell array of accelerometer data
Append calibration data from AccelSerials.csv file with calls to
AccelCalib().
Return accelArray
accelArray columns are: {1:DAQ ch, 2:serial number, 3:location name,
4:location axis, 5:polarity of axis, 6:calibration value}
%}

% Initialize variables.
delimiter = ',';
startRow = 4;

% Format string for each line of text:
%   column1: unsigned integer 8-bit
%	column2: unsigned integer 16-bit
%   column3: text (%s)
%	column4: text (%s)
%   column5: text (%s)
% DAQ ch., serial number, location, axis, polarity
formatSpec = '%u8%u16%s%s%s%[^\n\r]';
%formatSpec = '%s%s%s%s%s%[^\n\r]';

% Open the text file
fileID = fopen(filename,'r');

% Read columns of data according to format string
accelArray = textscan(fileID,formatSpec,...
    'Delimiter',delimiter,...
    'ReturnOnError',false,...
    'HeaderLines',startRow-1);

% Close the text file
fclose(fileID);

% number of accelerometers
numAccels=length(accelArray{1,1});

% accelerometer sensitivity values
sen = zeros(numAccels,1);

for AccelCounter=1:numAccels
    %sn = serial number from i row of accelArray
    sn = accelArray{1,2}(AccelCounter,1); 
    %call AccelCalib to get sensitivity value from AccelSerials.txt
    calibVal = AccelCalib(sn);
    % fill 'sen' array with sensitivity values
    sen(AccelCounter,1) = calibVal;
end

% append sen array to accelArray
accelArray{:,6} = sen;
end % end local function

  %% Local function to read accelerometer calibration .csv file
  function [calib] = AccelCalib(serialNum)
  % Read accelerometer information from AccelSerials.csv
  % Text file format: Model, serial number, sensitivity (V/g)
  % input: serial number
  % output: sensitivity returned in variable calib
    
  % Initialize variables.
  filename = '../configFiles/AccelSerials.csv';
  delimiter = ',';
  startRow = 4;

  % Format string for each line of text:
  % Model, Serial number, sensitivity (V/g)
  %	column2: double (%f)
  %   column3: double (%f)
  formatSpec = '%*s%f%f%[^\n\r]';

  % Open the text file.
  fileID = fopen(filename,'r');

  % Read columns of data according to format string.
  calibData = textscan(fileID,formatSpec,...
    'Delimiter',delimiter,...
    'HeaderLines',startRow-1,...
    'ReturnOnError',false);

  % Close the text file.
  fclose(fileID);

  % Find number of rows in matrix
  numUnits=length(calibData{1,1}(:,1));

  % set calib to zero
  % function will return 0 if serial number not found in file
  calib = 0;

  for unitCounter=1:numUnits
    if serialNum == calibData{1,1}(unitCounter,1)
        calib = calibData{1,2}(unitCounter,1);
    end
  end
end % end local function


function fsEntryCallback(~, ~, entry)
  % make sure a valid double has been passed
  % and its greater than 0
  str = entry.getText();
  val = str2double(str);
  if (~isnan(val))
      if (val < 0.0) 
          disp('Sampling frequency must be > 0.0');
          entry.restoreText();
      end
  else 
     disp(strcat(str, ' is not a valid sampling frequency'));
     entry.restoreText();
  end
end

function sdEntryCallback(~, ~, entry)
  % make sure a valid double has been passed
  % and its greater than 0
  str = entry.getText();
  val = str2double(str);
  if (~isnan(val))
      if (val < 0.0) 
          disp('Start delay must be >= 0.0');
          entry.restoreText();
      end
  else 
     disp(strcat(str, ' is not a valid start delay'));
     entry.restoreText();
  end
end

function edEntryCallback(~, ~, entry)
  % make sure a valid double has been passed
  % and its greater than 0
  str = entry.getText();
  val = str2double(str);
  if (~isnan(val))
      if (val < 0.0) 
          disp('End delay must be >= 0.0');
          entry.restoreText();
      end
  else 
     disp(strcat(str, ' is not a valid end delay'));
     entry.restoreText();
  end
end

function otEntryCallback(~, ~, entry)
  % make sure a valid double has been passed
  % and its greater than 0
  str = entry.getText();
  val = str2double(str);
  if (~isnan(val))
      if (val < 0.0) 
          disp('Output time must be > 0.0');
          entry.restoreText();
      end
  else 
     disp(strcat(str, ' is not a valid outout time'));
     entry.restoreText();
  end
end

function fileButtonCallback(~, ~, fileEntry)
  oldDir = cd('../configFiles');
  [file, ~, index] = uigetfile('*.csv', 'Select acclerometer configuration');
  if (index ~= 0) 
      fileEntry.setText(file);
  end
  cd(oldDir);
end

[velDat,dispDat] = discIntgrt(u,1000,4);

figure;
plot(t(1:end,1),u(1:end,1));
title('acceleration')

figure;
plot(t(1:end,1),velDat(1:end,1));
title('velocity')

figure;
plot(t(1:end,1),dispDat(1:end,1));
title('displacement')
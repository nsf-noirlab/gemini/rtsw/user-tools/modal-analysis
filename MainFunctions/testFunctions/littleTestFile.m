% Instructions:
% 1. Edit the ShakerCalcFRF() function by setting testMode = TRUE
%    (first line in function)
% 2. Run this script; it will rename a file, call ShakerCalcFRF() and save
%   the output in a file and then run plotFRF() which calls that file.

% load a real shaker file to start with.  This creates loads structure: sh
load('SHKR_20150413_20_49_23.mat');

% save the file with a new name so you don't mess up the original file
save('SHKR_testFile.mat','sh');

% run the FRF calculator on the test file
ShakerCalcFRF('SHKR_testFile.mat');
% in test mode, ShakerCalcFRF() will save the file as 'TestOut.mat'
% 'TestOut.mat' contains the structure: SHKR

% plot the test file
plotFRF2(5,150,1e-4,2e0,'TestOut.mat'); % plot routine

% create a spectrogram of the test file.
load('TestOut.mat');
figure(13);
spectrogram(sh.data(:,2),4096,[],4096,sh.fs) % original time data
title('Original Time Data');

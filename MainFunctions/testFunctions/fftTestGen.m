% test script - from reference [1]
% Produce a signal with two sinusoidal signals with known amplitude and a
% noise background with known spectral density.
% The frequencies of the sinusoidal signals are not harmonically related to
% each other nor are the related to the sample frequency.
% The noise background is simulated by rounding the time-series data to
% interger multiples of ulsb.
% The noise floor is thus: U(noise) = ulsb/(sqrt(6*fs)).
% For fs = 10kHz and ulsb = 1mV, the noise is ~4.08uV/sqrt(Hz).

fs = 2000;         % sample frequency (Hz)
f1 = 1234;          % first sine wave frequency (Hz)
amp1 = 2.828427;    % 2V RMS
f2 = 2500.2157;     % second sine wave frequency (Hz)
amp2 = 1;           % 0.707 Vrms
ulsb = 1e-3;        % value of 1 LSB in Volts

L = 1e6;            % length of data set
T=1/fs;             % sample time

t = (0:L-1)'*T;     % time vector

u = amp1*sin(2*pi*f1*t) + amp2*sin(2*pi*f2*t); % data vector
u = floor(u/ulsb + 0.5)*ulsb; % round off each value to LSB value (noise)

clear f1 amp1 f2 amp2 ulsb L T

%figure(43);
%plot(fs*t(1:1000,:),u(1:1000,:));

%   [1] Spectrum and spectral density estimation by the Discrete Fourier
%       transform(DFT), including a comprehensive list of window
%       functions and some new flat-top windows
%       G. Heinzel, et. al., Max-Plank-Institut fur Gravitationsphysik,
%       Teilinstitut Hannover, 2002
% test script - from reference [1]

% The amplitude of 1um pk displacement differentiated twice gives
% 1e-6 * (2*pi*f)^2 = 39.4784*f^2 = 3.948e-3 pk.

fs = 1000;         % sample frequency (Hz)
f1 = 10;          % first sine wave frequency (Hz)
amp1 = 3.948e-3;    % 1 um pk
%f2 = 250.2157;     % second sine wave frequency (Hz)
%amp2 = 2;        % 1 uV pk
%ulsb = 1e-9;        % value of 1 LSB in Volts

L = 1e6;            % length of data set
T=1/fs;             % sample time

plotTime = (0:L-1)'*T;     % time vector

% sine wave in x accelerometers
chan1 = 1;
chan2 = 5;
plotData = zeros(1e6,6);
plotData(:,chan1) = amp1*sin(2*pi*f1*plotTime);
plotData(:,chan2) = amp1*sin(2*pi*f1*plotTime);
%+ amp2*sin(2*pi*f2*t); % data vector
%u = floor(u/ulsb + 0.5)*ulsb; % round off each value to LSB value (noise)
save('T0_0_testdata_1um_pk_x.mat','plotData','plotTime');

% sine wave in y accelerometers
chan1 = 2;
chan2 = 3;
plotData = zeros(1e6,6);
plotData(:,chan1) = amp1*sin(2*pi*f1*plotTime);
plotData(:,chan2) = amp1*sin(2*pi*f1*plotTime);
save('T0_0_testdata_1um_pk_y.mat','plotData','plotTime');

% sine wave in z accelerometers
chan1 = 4;
chan2 = 6;
plotData = zeros(1e6,6);
plotData(:,chan1) = amp1*sin(2*pi*f1*plotTime);
plotData(:,chan2) = amp1*sin(2*pi*f1*plotTime);
save('T0_0_testdata_1um_pk_z.mat','plotData','plotTime');



%clear f1 amp1 f2 amp2 ulsb L T chan1 chan2
clear all

%figure(43);
%plot(fs*t(1:1000,:),u(1:1000,:));

%   [1] Spectrum and spectral density estimation by the Discrete Fourier
%       transform(DFT), including a comprehensive list of window
%       functions and some new flat-top windows
%       G. Heinzel, et. al., Max-Plank-Institut fur Gravitationsphysik,
%       Teilinstitut Hannover, 2002